
root_dir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

#vpath %.c $(root_dir)src

CXXFLAGS ?= -g
CPPFLAGS ?= $(shell pkg-config --cflags readline)
LDFLAGS ?=
LDLIBS ?= $(shell pkg-config --libs readline)

YACC = bison

override CXXFLAGS += -std=c++20
override CXXFLAGS += -fPIC
override CXXFLAGS += -MMD
override CXXFLAGS += -Wall
override CXXFLAGS += -Werror
override CXXFLAGS += -Wfatal-errors

override CPPFLAGS += -I$(root_dir)

override LDLIBS += -lstdc++

# Do not allow direct compilation of % from %.cc; it breaks -MMD

%: %.cc
%.c: %.l
%.c: %.y

%.cc: %.l
	$(LEX) -o $@ $< 

%.cc: %.y
	$(YACC) -d -o $@ $< 

%.a:
	$(AR) rcs $@ $^

tests += tests/hello.shon
tests += tests/import.shon
tests += tests/print.shon
tests += tests/function.shon
tests += tests/closure.shon
tests += tests/cfunction.shon
tests += tests/conditional.shon
tests += tests/nested.shon
tests += tests/while.shon
tests += tests/deep.shon
tests += tests/arguments.shon
tests += tests/equality.shon
tests += tests/relational.shon
tests += tests/logical1.shon
tests += tests/logical2.shon
tests += tests/logical3.shon
tests += tests/factorial.shon
tests += tests/factorial2.shon
tests += tests/blank_lines.shon
tests += tests/string.shon
tests += tests/self_dependant_expr.shon
tests += tests/concatenate_strings.shon
tests += tests/list_access.shon
tests += tests/list_modification.shon
tests += tests/list_iteration.shon
tests += tests/list_concatenation.shon
tests += tests/list_exporting.shon
tests += tests/dict_access.shon
tests += tests/dict_modification.shon
tests += tests/dict_iteration.shon
tests += tests/dict_concatenation.shon
tests += tests/dict_exporting.shon
tests += tests/dict_shorthand.shon
tests += tests/non_destructive_iteration.shon
tests += tests/type.shon

.PHONY: main
all: shon/scanner.cc shon/parser.cc tests/tree tests/state tests/parse shon/shon.a tests/main 

.PHONY: check
check: all
	@for i in $(tests); do\
		if tests/main $$i > /dev/null; then\
			echo "\033[0;32mpass: \033[1;32m$$i\033[0;0m"; \
		else\
			echo "\033[0;31mfail: \033[1;31m$$i\033[0;0m"; \
		fi;\
	done

.PHONY: clean
clean:
	$(RM) */*.o
	$(RM) */*.d
	$(RM) shon/scanner.cc
	$(RM) shon/parser.cc
	$(RM) shon/parser.hh
	$(RM) shon/shon.a
	$(RM) tests/main

tests/main: tests/main.o shon/shon.a

tests/parse: tests/parse.o shon/scanner.o shon/parser.o

shon/shon.a: shon/scanner.o shon/parser.o shon/prune.o shon/flatten.o shon/compiler.o shon/interp.o

-include $(shell find -name "*.d")

