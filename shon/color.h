
#ifndef SHON_COLOR_H_
#define SHON_COLOR_H_

#include <ostream>

namespace shon {

inline std::string colorize(std::string_view in)
{
        // groups:
        // 0. other
        // 1. space
        // 2. digit
        // 3. letter
        // 4. symbol
        // 5. quote
        // 6. backlash

        static constexpr const int group_[] = {
                0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                1, 4, 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
                2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 4, 4,
                4, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
                3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 6, 4, 4, 3,
                4, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
                3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        };

        //        ? WS DG AL SY QT BS
        static constexpr const int trans_[9][7] = {
                { 8, 1, 3, 2, 1, 4, 8 }, // initial
                { 8, 1, 3, 2, 1, 4, 8 }, // other
                { 8, 1, 2, 2, 1, 4, 8 }, // tag
                { 8, 1, 3, 3, 1, 4, 8 }, // num
                { 8, 4, 4, 4, 4, 5, 6 }, // string
                { 8, 1, 3, 4, 1, 4, 8 }, // string end
                { 7, 7, 7, 7, 7, 7, 7 }, // escape
                { 8, 4, 4, 4, 4, 5, 6 }, // escape end
                { 8, 8, 8, 8, 8, 8, 8 }, // error
        };

        std::stringstream out;
        int state = 0;
        for (auto x : in) {
                int old_state = state;
                state = trans_[state][group_[static_cast<int>(x)]];
                if (state != old_state) {
                        switch (state) {
                         case 1: out << "\e[0;33m"; break;
                         case 2: out << "\e[0;34m"; break;
                         case 3: out << "\e[0;36m"; break;
                         case 4: out << "\e[0;36m"; break;
                         case 5: out << "\e[0;36m"; break;
                         case 6: out << "\e[0;31m"; break;
                         case 7: out << "\e[0;31m"; break;
                         case 8: out << "\e[0;41m"; break;
                        }
                }
                out << x;
        }
        out << "\e[0;0m";
        return out.str();
}

}; // namespace shon

#endif // SHON_COLOR_H_
