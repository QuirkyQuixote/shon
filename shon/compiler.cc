
#include "compiler.h"

#include <optional>
#include <ranges>
#include <iostream>

#include "color.h"

namespace shon {

class Chunk_ostream {
 private:
        std::unique_ptr<std::byte[]> data;
        std::byte* pos{nullptr};
        std::byte* end{nullptr};
        std::byte* max{nullptr};

 public:
        std::byte* get() const { return data.get(); }
        std::byte* release() { return data.release(); }

        size_t tellp() const { return pos - get(); }
        size_t size() const { return end - get(); }
        size_t allocated() const { return max - get(); }

        void reserve(size_t new_max)
        {
                size_t cur_max = allocated();
                if (new_max <= cur_max)
                        return;
                if (cur_max == 0)
                        cur_max = 1024;
                while (cur_max < new_max)
                        cur_max *= 2;
                std::byte* new_data = new std::byte[cur_max];
                std::copy_n(get(), size(), new_data);
                pos = new_data + tellp();
                end = new_data + size();
                max = new_data + cur_max;
                data.reset(new_data);
        }

        void resize(size_t new_size)
        {
                reserve(new_size);
                end = get() + new_size;
        }

        void seekp(size_t n) { pos = get() + n; }

        template<class T> Chunk_ostream& operator<<(T x)
        {
                reserve(tellp() + sizeof(T));
                std::copy_n(reinterpret_cast<const std::byte*>(&x), sizeof(T), pos);
                pos += sizeof(T);
                end = std::max(pos, end);
                return *this;
        }
};

struct Label {
        std::vector<size_t> jumps;
        size_t target;
};

class Compile {
 private:
        using tuple_type = std::ranges::subrange<const flat::val*>;
        Chunk_ostream code_;
        std::vector<Label> labels_;

        const Flat_ast& i_;
        State& o_;

        void args_(tuple_type t)
        {
                code_ << static_cast<uint8_t>(t.size() - 1);
                for (auto r : t | std::views::drop(1))
                        value_(get_tuple(i_.pool, r));
        }

        void call_(tuple_type t)
        {
                code_ << Instr::call;
                value_(get_tuple(i_.pool, t[1]));
                value_(get_tuple(i_.pool, t[2]));
                args_(get_tuple(i_.pool, t[3]));
        }

        void list_(tuple_type t)
        {
                code_ << Instr::list;
                args_(get_tuple(i_.pool, t[2]));
                value_(get_tuple(i_.pool, t[1]));
        }

        void dict_(tuple_type t)
        {
                code_ << Instr::dict;
                args_(get_tuple(i_.pool, t[2]));
                value_(get_tuple(i_.pool, t[1]));
        }

        void pair_(tuple_type t)
        {
                value_(get_tuple(i_.pool, t[1]));
                value_(get_tuple(i_.pool, t[2]));
        }

        void local_(tuple_type t)
        {
                uint16_t i = get_number(t[1]);
                code_ << i;
        }

        void global_(tuple_type t)
        {
                uint16_t i = ~get_number(t[1]);
                code_ << i;
        }

        void value_(tuple_type t)
        {
                switch (get_tag(t[0])) {
                 case flat::tag::local_: local_(t); return;
                 case flat::tag::global_: global_(t); return;
                 case flat::tag::pair_: pair_(t); return;
                 default: break;
                }
                std::stringstream message;
                message << "expected value, got " << get_tag(t[0]) <<
                        " instead";
                throw std::runtime_error{message.str()};
        }

        void label_target_(tuple_type t)
        {
                size_t l = get_number(t[1]);
                if (l >= labels_.size()) labels_.resize(l + 1);
                labels_[l].target = code_.tellp();
        }

        void label_jump_(tuple_type t)
        {
                size_t l = get_number(t[1]);
                if (l >= labels_.size()) labels_.resize(l + 1);
                labels_[l].jumps.push_back(code_.tellp());
                code_ << static_cast<uint16_t>(0);
        }

        void jmp_(tuple_type t)
        {
                code_ << Instr::jmp;
                label_jump_(get_tuple(i_.pool, t[1]));
        }

        void jt_(tuple_type t)
        {
                code_ << Instr::jt;
                value_(get_tuple(i_.pool, t[1]));
                label_jump_(get_tuple(i_.pool, t[2]));
        }

        void jf_(tuple_type t)
        {
                code_ << Instr::jf;
                value_(get_tuple(i_.pool, t[1]));
                label_jump_(get_tuple(i_.pool, t[2]));
        }

        void jeq_(tuple_type t)
        {
                code_ << Instr::jeq;
                value_(get_tuple(i_.pool, t[1]));
                value_(get_tuple(i_.pool, t[2]));
                label_jump_(get_tuple(i_.pool, t[3]));
        }

        void jne_(tuple_type t)
        {
                code_ << Instr::jne;
                value_(get_tuple(i_.pool, t[1]));
                value_(get_tuple(i_.pool, t[2]));
                label_jump_(get_tuple(i_.pool, t[3]));
        }

        void jlt_(tuple_type t)
        {
                code_ << Instr::jlt;
                value_(get_tuple(i_.pool, t[1]));
                value_(get_tuple(i_.pool, t[2]));
                label_jump_(get_tuple(i_.pool, t[3]));
        }

        void jgt_(tuple_type t)
        {
                code_ << Instr::jgt;
                value_(get_tuple(i_.pool, t[1]));
                value_(get_tuple(i_.pool, t[2]));
                label_jump_(get_tuple(i_.pool, t[3]));
        }

        void jle_(tuple_type t)
        {
                code_ << Instr::jle;
                value_(get_tuple(i_.pool, t[1]));
                value_(get_tuple(i_.pool, t[2]));
                label_jump_(get_tuple(i_.pool, t[3]));
        }

        void jge_(tuple_type t)
        {
                code_ << Instr::jge;
                value_(get_tuple(i_.pool, t[1]));
                value_(get_tuple(i_.pool, t[2]));
                label_jump_(get_tuple(i_.pool, t[3]));
        }

        void cp_(tuple_type t)
        {
                code_ << Instr::cp;
                value_(get_tuple(i_.pool, t[2]));
                value_(get_tuple(i_.pool, t[1]));
        }

        void mul_(tuple_type t)
        {
                code_ << Instr::mul;
                value_(get_tuple(i_.pool, t[2]));
                value_(get_tuple(i_.pool, t[3]));
                value_(get_tuple(i_.pool, t[1]));
        }

        void div_(tuple_type t)
        {
                code_ << Instr::div;
                value_(get_tuple(i_.pool, t[2]));
                value_(get_tuple(i_.pool, t[3]));
                value_(get_tuple(i_.pool, t[1]));
        }

        void mod_(tuple_type t)
        {
                code_ << Instr::mod;
                value_(get_tuple(i_.pool, t[2]));
                value_(get_tuple(i_.pool, t[3]));
                value_(get_tuple(i_.pool, t[1]));
        }

        void add_(tuple_type t)
        {
                code_ << Instr::add;
                value_(get_tuple(i_.pool, t[2]));
                value_(get_tuple(i_.pool, t[3]));
                value_(get_tuple(i_.pool, t[1]));
        }

        void sub_(tuple_type t)
        {
                code_ << Instr::sub;
                value_(get_tuple(i_.pool, t[2]));
                value_(get_tuple(i_.pool, t[3]));
                value_(get_tuple(i_.pool, t[1]));
        }

        void shl_(tuple_type t)
        {
                code_ << Instr::shl;
                value_(get_tuple(i_.pool, t[2]));
                value_(get_tuple(i_.pool, t[3]));
                value_(get_tuple(i_.pool, t[1]));
        }

        void shr_(tuple_type t)
        {
                code_ << Instr::shr;
                value_(get_tuple(i_.pool, t[2]));
                value_(get_tuple(i_.pool, t[3]));
                value_(get_tuple(i_.pool, t[1]));
        }

        void neg_(tuple_type t)
        {
                code_ << Instr::neg;
                value_(get_tuple(i_.pool, t[2]));
                value_(get_tuple(i_.pool, t[1]));
        }

        void and_(tuple_type t)
        {
                code_ << Instr::binary_and;
                value_(get_tuple(i_.pool, t[2]));
                value_(get_tuple(i_.pool, t[3]));
                value_(get_tuple(i_.pool, t[1]));
        }

        void xor_(tuple_type t)
        {
                code_ << Instr::binary_xor;
                value_(get_tuple(i_.pool, t[2]));
                value_(get_tuple(i_.pool, t[3]));
                value_(get_tuple(i_.pool, t[1]));
        }

        void or_(tuple_type t)
        {
                code_ << Instr::binary_or;
                value_(get_tuple(i_.pool, t[2]));
                value_(get_tuple(i_.pool, t[3]));
                value_(get_tuple(i_.pool, t[1]));
        }

        void not_(tuple_type t)
        {
                code_ << Instr::binary_not;
                value_(get_tuple(i_.pool, t[2]));
                value_(get_tuple(i_.pool, t[1]));
        }

        void begin_(tuple_type t)
        {
                code_ << Instr::begin;
                value_(get_tuple(i_.pool, t[2]));
                value_(get_tuple(i_.pool, t[1]));
        }

        void end_(tuple_type t)
        {
                code_ << Instr::end;
                value_(get_tuple(i_.pool, t[2]));
                value_(get_tuple(i_.pool, t[1]));
        }

        void advance_(tuple_type t)
        {
                code_ << Instr::adv;
                value_(get_tuple(i_.pool, t[1]));
        }

        void deref_(tuple_type t)
        {
                code_ << Instr::deref;
                value_(get_tuple(i_.pool, t[2]));
                value_(get_tuple(i_.pool, t[1]));
        }

        void ldi_(tuple_type t)
        {
                code_ << Instr::ldi;
                value_(get_tuple(i_.pool, t[2]));
                value_(get_tuple(i_.pool, t[3]));
                value_(get_tuple(i_.pool, t[1]));
        }

        void sti_(tuple_type t)
        {
                code_ << Instr::sti;
                value_(get_tuple(i_.pool, t[1]));
                value_(get_tuple(i_.pool, t[2]));
                value_(get_tuple(i_.pool, t[3]));
        }

        void ret_(tuple_type t)
        {
                code_ << Instr::ret;
                value_(get_tuple(i_.pool, t[1]));
        }

        void bind_(tuple_type t)
        {
                code_ << Instr::bind;
                value_(get_tuple(i_.pool, t[2]));
                value_(get_tuple(i_.pool, t[1]));
        }

        void statement_(tuple_type t)
        {
                switch (get_tag(t[0])) {
                 case flat::tag::list_: list_(t); return;
                 case flat::tag::dict_: dict_(t); return;
                 case flat::tag::local_: local_(t); return;
                 case flat::tag::global_: global_(t); return;
                 case flat::tag::label_: label_target_(t); return;
                 case flat::tag::jt_: jt_(t); return;
                 case flat::tag::jf_: jf_(t); return;
                 case flat::tag::jeq_: jeq_(t); return;
                 case flat::tag::jne_: jne_(t); return;
                 case flat::tag::jlt_: jlt_(t); return;
                 case flat::tag::jgt_: jgt_(t); return;
                 case flat::tag::jle_: jle_(t); return;
                 case flat::tag::jge_: jge_(t); return;
                 case flat::tag::jmp_: jmp_(t); return;
                 case flat::tag::cp_: cp_(t); return;
                 case flat::tag::mul_: mul_(t); return;
                 case flat::tag::div_: div_(t); return;
                 case flat::tag::mod_: mod_(t); return;
                 case flat::tag::add_: add_(t); return;
                 case flat::tag::sub_: sub_(t); return;
                 case flat::tag::shl_: shl_(t); return;
                 case flat::tag::shr_: shr_(t); return;
                 case flat::tag::neg_: neg_(t); return;
                 case flat::tag::and_: and_(t); return;
                 case flat::tag::xor_: xor_(t); return;
                 case flat::tag::or_: or_(t); return;
                 case flat::tag::not_: not_(t); return;
                 case flat::tag::call_: call_(t); return;
                 case flat::tag::begin_: begin_(t); return;
                 case flat::tag::end_: end_(t); return;
                 case flat::tag::advance_: advance_(t); return;
                 case flat::tag::deref_: deref_(t); return;
                 case flat::tag::ldi_: ldi_(t); return;
                 case flat::tag::sti_: sti_(t); return;
                 case flat::tag::ret_: ret_(t); return;
                 case flat::tag::bind_: bind_(t); return;
                 default: break;
                }
                std::stringstream message;
                message << "expected statement, got " << get_tag(t[0]) <<
                        " instead";
                throw std::runtime_error{message.str()};
        }

        void chunk_(tuple_type t)
        {
                size_t n = get_number(t[0]);
                for (auto r : t | std::views::drop(1))
                        statement_(get_tuple(i_.pool, r));
                for (auto& l : labels_) {
                        for (auto j : l.jumps) {
                                code_.seekp(j);
                                code_ << static_cast<uint16_t>(l.target);
                        }
                }
                o_.globals[n] = o_(code_.release());
        }

 public:
        Compile(const Flat_ast& i, State& o) : i_{i}, o_{o} {}

        void operator()(const flat::val& r)
        {
                chunk_(get_tuple(i_.pool, r));
        }
};

void compile(const Flat_ast& i, State& o)
{
        for (auto r : i.roots)
                Compile{i, o}(r);
}

}; // namespace shon

