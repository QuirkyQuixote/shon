
#ifndef SHON_COMPILER_H_
#define SHON_COMPILER_H_

#include "flatten.h"
#include "state.h"

#include <ostream>
#include <iomanip>
#include <map>

namespace shon {

void compile(const Flat_ast& t, State& p);

}; // namespace shon

#endif // SHON_COMPILER_H_
