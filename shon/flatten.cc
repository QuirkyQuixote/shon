
#include "flatten.h"

#include <optional>
#include <ranges>
#include <iostream>

namespace shon {

class Flatten {
 private:
        const Tagged_ast& i_;
        Flat_ast &o_;
        State& s_;
        std::vector<flat::val> func_;
        std::vector<size_t> frame_end_;
        size_t next_var_ = 0;
        size_t next_label_ = 0;
        size_t next_global_ = 0;

        void require_(ast::val a, ast::tag t)
        {
                if (get_tag(a) == t) return;
                std::stringstream message;
                message << "shon::Flatten: expected ";
                message << t;
                message << ", got ";
                message << get_tag(a);
                message << " instead";
                throw std::runtime_error{message.str()};
        }

        void push_scope_()
        {
                frame_end_.push_back(frame_end_.back());
                next_var_ = frame_end_.back();
        }

        void pop_scope_()
        {
                frame_end_.pop_back();
                next_var_ = frame_end_.back();
        }

        flat::val push_var_()
        { return o_.pool(o_.pool(flat::tag::local_), o_.pool(next_var_++)); }

        flat::val push_label_()
        { return o_.pool(o_.pool(flat::tag::label_), o_.pool(next_label_++)); }

        flat::val identifier_(ast::val a)
        {
                auto c = get_tuple(i_.pool, a);
                size_t n = get_number(c[1]);
                frame_end_.back() = std::max<size_t>(n + 1, frame_end_.back());
                next_var_ = std::max<size_t>(n + 1, next_var_);
                return o_.pool(o_.pool(flat::tag::local_), o_.pool(n));
        }

        flat::val global_(val g)
        {
                auto f = [this,&g](const val& x){ return equal_to(s_)(x, g); };
                auto i = std::ranges::find_if(s_.globals, f);
                if (i == s_.globals.end())
                        i = s_.globals.insert(i, g);
                return o_.pool(o_.pool(flat::tag::global_),
                                o_.pool(i - s_.globals.begin()));
        }

        flat::val nil_() { return global_(s_()); }

        flat::val bool_(bool b) { return global_(s_(b)); }

        flat::val number_(ast::val a)
        {
                auto c = get_tuple(i_.pool, a);
                return global_(s_(get_number(c[1])));
        }

        flat::val string_(ast::val a)
        {
                auto c = get_tuple(i_.pool, a);
                return global_(s_(get_string(i_.pool, c[1])));
        }

        flat::val assign_expression_(ast::val a)
        {
                auto c = get_tuple(i_.pool, a);
                if (is_object(i_.pool, c[1], ast::tag::idx_expr)) {
                        auto cc = get_tuple(i_.pool, c[1]);
                        auto r1 = expression_(cc[1]);
                        auto r2 = expression_(cc[2]);
                        auto r3 = expression_(c[2]);
                        func_.push_back(o_.pool(o_.pool(flat::tag::sti_), r1, r2, r3));
                        return r3;
                }
                auto l = expression_(c[1]);
                auto r = expression_(c[2]);
                func_.push_back(o_.pool(o_.pool(flat::tag::cp_), l, r));
                return l;
        }

        flat::val unary_expression_(ast::val a, flat::tag tok)
        {
                auto c = get_tuple(i_.pool, a);
                auto l = push_var_();
                auto r = expression_(c[1]);
                func_.push_back(o_.pool(o_.pool(tok), l, r));
                return l;
        }

        flat::val binary_expression_(ast::val a, flat::tag tok)
        {
                auto c = get_tuple(i_.pool, a);
                auto l = push_var_();
                auto r1 = expression_(c[1]);
                auto r2 = expression_(c[2]);
                func_.push_back(o_.pool(o_.pool(tok), l, r1, r2));
                return l;
        }

        flat::val call_argument_list_(ast::val a)
        {
                auto c = get_tuple(i_.pool, a);
                std::vector<flat::val> buf{o_.pool(flat::tag::args_)};
                for (auto x : c | std::views::drop(1))
                        buf.push_back(expression_(x));
                return o_.pool(buf);
        }

        flat::val pair_expression_(ast::val a)
        {
                auto c = get_tuple(i_.pool, a);
                auto r1 = expression_(c[1]);
                auto r2 = expression_(c[2]);
                return o_.pool(o_.pool(flat::tag::pair_), r1, r2);
        }

        flat::val tagged_argument_list_(ast::val a)
        {
                auto c = get_tuple(i_.pool, a);
                std::vector<flat::val> buf{o_.pool(flat::tag::args_)};
                for (auto x : c | std::views::drop(1))
                        buf.push_back(pair_expression_(x));
                return o_.pool(buf);
        }

        flat::val logical_get_rvalue_(ast::val a)
        {
                auto s = push_var_();
                auto l = push_label_();
                func_.push_back(o_.pool(o_.pool(flat::tag::cp_), s, bool_(false)));
                logical_expression_(a, l);
                func_.push_back(o_.pool(o_.pool(flat::tag::cp_), s, bool_(true)));
                func_.push_back(l);
                return s;
        }

        flat::val expression_(ast::val a)
        {
                auto c = get_tuple(i_.pool, a);
                switch (get_tag(c[0])) {
                 case ast::tag::nil: return nil_();
                 case ast::tag::identifier: return identifier_(a);
                 case ast::tag::number: return number_(a);
                 case ast::tag::string: return string_(a);

                 case ast::tag::assign_expr: return assign_expression_(a);

                 case ast::tag::mul_expr: return binary_expression_(a, flat::tag::mul_);
                 case ast::tag::div_expr: return binary_expression_(a, flat::tag::div_);
                 case ast::tag::mod_expr: return binary_expression_(a, flat::tag::mod_);
                 case ast::tag::add_expr: return binary_expression_(a, flat::tag::add_);
                 case ast::tag::sub_expr: return binary_expression_(a, flat::tag::sub_);
                 case ast::tag::shl_expr: return binary_expression_(a, flat::tag::shl_);
                 case ast::tag::shr_expr: return binary_expression_(a, flat::tag::shr_);
                 case ast::tag::neg_expr: return unary_expression_(a, flat::tag::neg_);

                 case ast::tag::band_expr: return binary_expression_(a, flat::tag::and_);
                 case ast::tag::bxor_expr: return binary_expression_(a, flat::tag::xor_);
                 case ast::tag::bor_expr: return binary_expression_(a, flat::tag::or_);
                 case ast::tag::bnot_expr: return unary_expression_(a, flat::tag::not_);

                 case ast::tag::call_expr: return binary_expression_(a, flat::tag::call_);
                 case ast::tag::list_expr: return unary_expression_(a, flat::tag::list_);
                 case ast::tag::dict_expr: return unary_expression_(a, flat::tag::dict_);

                 case ast::tag::idx_expr: return binary_expression_(a, flat::tag::ldi_);
                 case ast::tag::pair_expr: return pair_expression_(a);

                 case ast::tag::call_argument_list: return call_argument_list_(a);
                 case ast::tag::tagged_argument_list: return tagged_argument_list_(a);

                 case ast::tag::eq_expr:
                 case ast::tag::ne_expr:
                 case ast::tag::lt_expr:
                 case ast::tag::gt_expr:
                 case ast::tag::le_expr:
                 case ast::tag::ge_expr:
                 case ast::tag::land_expr:
                 case ast::tag::lor_expr:
                 case ast::tag::lnot_expr: return logical_get_rvalue_(a);

                 default: throw std::bad_cast{};
                }
        }

        void relational_expression_(ast::val a, flat::tag tok, flat::val fail)
        {
                auto c = get_tuple(i_.pool, a);
                auto l = expression_(c[1]);
                auto r = expression_(c[2]);
                func_.push_back(o_.pool(o_.pool(tok), l, r, fail));
        }

        void rvalue_get_logical_(ast::val a, flat::val fail)
        {
                auto l = expression_(a);
                func_.push_back(o_.pool(o_.pool(flat::tag::jf_), l, fail));
        }

        void logical_and_expression_(ast::val a, flat::val fail)
        {
                auto c = get_tuple(i_.pool, a);
                logical_expression_(c[1], fail);
                logical_expression_(c[2], fail);
        }

        void logical_not_expression_(ast::val a, flat::val fail)
        {
                auto c = get_tuple(i_.pool, a);
                auto succeed = push_label_();
                logical_expression_(c[1], succeed);
                func_.push_back(o_.pool(o_.pool(flat::tag::jmp_), fail));
                func_.push_back(succeed);
        }

        void logical_expression_(ast::val a, flat::val fail)
        {
                auto c = get_tuple(i_.pool, a);
                switch (get_tag(c[0])) {
                 case ast::tag::eq_expr: relational_expression_(a, flat::tag::jne_, fail); return;
                 case ast::tag::ne_expr: relational_expression_(a, flat::tag::jeq_, fail); return;
                 case ast::tag::lt_expr: relational_expression_(a, flat::tag::jge_, fail); return;
                 case ast::tag::gt_expr: relational_expression_(a, flat::tag::jle_, fail); return;
                 case ast::tag::le_expr: relational_expression_(a, flat::tag::jgt_, fail); return;
                 case ast::tag::ge_expr: relational_expression_(a, flat::tag::jlt_, fail); return;

                 case ast::tag::land_expr: logical_and_expression_(a, fail); return;
                 case ast::tag::lnot_expr: logical_not_expression_(a, fail); return;

                 default: rvalue_get_logical_(a, fail); return;
                }
        }

        void expression_statement_(ast::val a)
        {
                expression_(a);
                next_var_ = frame_end_.back();
        }

        void return_statement_(ast::val a)
        {
                auto c = get_tuple(i_.pool, a);
                require_(c[0], ast::tag::return_statement);
                if (c.size() > 1) {
                        auto l = expression_(c[1]);
                        func_.push_back(o_.pool(o_.pool(flat::tag::ret_), l));
                } else {
                        func_.push_back(o_.pool(o_.pool(flat::tag::ret_), nil_()));
                }
        }

        void for_statement_(ast::val a)
        {
                auto c = get_tuple(i_.pool, a);
                push_scope_();
                auto iter = identifier_(c[1]);
                auto last = identifier_(c[2]);
                auto var = identifier_(c[3]);
                frame_end_.back() = next_var_;
                auto range = expression_(c[4]);
                auto head = push_label_();
                auto foot = push_label_();
                next_var_ = (frame_end_.back() += 2);
                func_.push_back(o_.pool(o_.pool(flat::tag::begin_), iter, range));
                func_.push_back(o_.pool(o_.pool(flat::tag::end_), last, range));
                func_.push_back(head);
                func_.push_back(o_.pool(o_.pool(flat::tag::jeq_), iter, last, foot));
                func_.push_back(o_.pool(o_.pool(flat::tag::deref_), var, iter));
                statement_list_(c[5]);
                func_.push_back(o_.pool(o_.pool(flat::tag::advance_), iter));
                func_.push_back(o_.pool(o_.pool(flat::tag::jmp_), head));
                func_.push_back(foot);
                pop_scope_();
        }

        void while_statement_(ast::val a)
        {
                auto c = get_tuple(i_.pool, a);
                auto head = push_label_();
                auto foot = push_label_();
                func_.push_back(head);
                push_scope_();
                logical_expression_(c[1], foot);
                statement_list_(c[2]);
                pop_scope_();
                func_.push_back(o_.pool(o_.pool(flat::tag::jmp_), head));
                func_.push_back(foot);
        }

        void else_statement_(ast::val a)
        {
                auto c = get_tuple(i_.pool, a);
                switch (get_tag(c[0])) {
                 case ast::tag::if_statement:
                        if_statement_(a);
                        return;
                 case ast::tag::statement_list:
                        statement_list_(a);
                        return;
                 case ast::tag::empty_statement:
                        return;
                 default:
                        throw std::bad_cast{};
                }
        }

        void if_statement_(ast::val a)
        {
                auto c = get_tuple(i_.pool, a);
                require_(c[0], ast::tag::if_statement);
                auto next = push_label_();
                auto foot = push_label_();
                push_scope_();
                logical_expression_(c[1], next);
                statement_list_(c[2]);
                pop_scope_();
                func_.push_back(o_.pool(o_.pool(flat::tag::jmp_), foot));
                func_.push_back(next);
                else_statement_(c[3]);
                func_.push_back(foot);
        }

        void import_statement_(ast::val a)
        {
                auto c = get_tuple(i_.pool, a);
                auto i = identifier_(c[1]);
                auto g = s_.get(get_string(i_.pool, c[2]));
                auto s = global_(g);
                func_.push_back(o_.pool(o_.pool(flat::tag::cp_), i, s));
        }

        void function_argument_(ast::val a)
        {
                auto c = get_tuple(i_.pool, a);
                require_(c[0], ast::tag::identifier);
        }

        void function_argument_list_(ast::val a)
        {
                auto c = get_tuple(i_.pool, a);
                require_(c[0], ast::tag::function_argument_list);
                for (auto r : c | std::views::drop(1))
                        function_argument_(r);
        }

        void function_statement_(ast::val a)
        {
                auto c = get_tuple(i_.pool, a);
                std::vector<flat::val> func;
                std::swap(func, func_);
                auto i = identifier_(c[1]);
                push_scope_();
                function_argument_list_(c[2]);
                auto n = s_.globals.size();
                chunk_(c[3]);
                pop_scope_();
                o_.roots.push_back(o_.pool(func_));
                std::swap(func, func_);
                auto g = o_.pool(o_.pool(flat::tag::global_), o_.pool(n));
                func_.push_back(o_.pool(o_.pool(flat::tag::bind_), i, g));
        }

        void statement_(ast::val a)
        {
                auto c = get_tuple(i_.pool, a);
                if (get_tag(c[0]) == ast::tag::if_statement)
                        if_statement_(a);
                else if (get_tag(c[0]) == ast::tag::while_statement)
                        while_statement_(a);
                else if (get_tag(c[0]) == ast::tag::for_statement)
                        for_statement_(a);
                else if (get_tag(c[0]) == ast::tag::return_statement)
                        return_statement_(a);
                else if (get_tag(c[0]) == ast::tag::import)
                        import_statement_(a);
                else if (get_tag(c[0]) == ast::tag::function)
                        function_statement_(a);
                else
                        expression_statement_(a);
        }

        void statement_list_(ast::val a)
        {
                auto c = get_tuple(i_.pool, a);
                require_(c[0], ast::tag::statement_list);
                for (auto r : c | std::views::drop(1))
                        statement_(r);
        }

        void chunk_(ast::val a)
        {
                func_.push_back(o_.pool(s_.globals.size()));
                s_.globals.emplace_back();
                statement_list_(a);
                func_.push_back(o_.pool(o_.pool(flat::tag::ret_), nil_()));
        }

 public:
        Flatten(const Tagged_ast& p, Flat_ast& o, State& s) : i_{p}, o_{o}, s_{s} {}

        void operator()()
        {
                frame_end_.push_back(0);
                chunk_(i_.root);
                o_.roots.push_back(o_.pool(func_));
        }
};

Flat_ast flatten(const Tagged_ast& i, State& s)
{
        Flat_ast o;
        Flatten{i, o, s}();
        return o;
}

}; // namespace shon

