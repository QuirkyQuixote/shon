
#ifndef SHON_FLATTEN_H_
#define SHON_FLATTEN_H_

#include "prune.h"
#include "state.h"

#include <ostream>
#include <iomanip>
#include <map>

namespace shon {

namespace flat {

enum class tag : int16_t {
        nil_, number_, string_, list_, dict_, pair_, bind_,
        local_, global_, label_,
        jeq_, jne_, jlt_, jgt_, jle_, jge_, jmp_, jf_, jt_, cp_, ldi_, sti_,
        mul_, div_, mod_, add_, sub_, shl_, shr_, neg_, and_, xor_, or_, not_,
        call_, args_, begin_, end_, advance_, deref_, ret_,
        count_
};

inline std::ostream& operator<<(std::ostream& os, tag t)
{
        static const char* names_[] = {
                "nil", "number", "string", "list", "dict", "pair", "bind",
                "local", "global", "label", "jeq", "jne", "jlt", "jgt", "jle",
                "jge", "jmp", "jf", "jt", "cp", "ldi", "sti", "mul", "div",
                "mod", "add", "sub", "shl", "shr", "neg", "and", "xor", "or",
                "not", "call", "args", "begin", "end", "advance", "deref",
                "ret"
        };
        if (t < tag::count_)
                return os << names_[static_cast<size_t>(t)];
        return os << static_cast<size_t>(t);
}

using pool = Tree<tag>;
using val = Val<tag>;

}; // namespace flat

struct Flat_ast {
        flat::pool pool;
        std::vector<flat::val> roots;
};

struct Flat_ostream {
        std::ostream& os;
        const Flat_ast& f;

        void tuple_(const flat::val& r)
        {
                auto t = get_tuple(f.pool, r);
                if (!t.empty() && t[0].type() == Payload::tag) {
                        if (get_tag(t[0]) == flat::tag::global_) {
                                os << "#" << get_number(t[1]);
                                return;
                        } else if (get_tag(t[0]) == flat::tag::local_) {
                                os << "$" << get_number(t[1]);
                                return;
                        } else if (get_tag(t[0]) == flat::tag::label_) {
                                os << "L" << get_number(t[1]);
                                return;
                        }
                }
                os << "(";
                const char* separator = "";
                for (auto r : t) {
                        os << separator;
                        separator = " ";
                        *this << r;
                }
                os << ")";
        }

        Flat_ostream& operator<<(const flat::val& r)
        {
                if (r.type() == Payload::tag) {
                        os << get_tag(r);
                } else if (r.type() == Payload::number) {
                        os << get_number(r);
                } else if (r.type() == Payload::pointer) {
                        os << get_pointer(r);
                } else if (r.type() == Payload::string) {
                        os << '"' << get_string(f.pool, r) << '"';
                } else if (r.type() == Payload::tuple) {
                        tuple_(r);
                } else {
                        throw std::bad_cast{};
                }
                return *this;
        }
};

inline Flat_ostream operator<<(std::ostream& os, const Flat_ast& f)
{ return Flat_ostream{os, f}; }

inline std::string to_string(const Flat_ast& f, const flat::val& r)
{
        std::stringstream solution;
        solution << f << r;
        return solution.str();
}

Flat_ast flatten(const Tagged_ast& t, State& s);

}; // namespace shon

#endif // SHON_FLATTEN_H_
