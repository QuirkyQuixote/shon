
#include "interp.h"

#include <iostream>
#include <algorithm>

#include "color.h"

namespace shon {

class Chunk_istream {
 private:
        std::byte* chunk;
        std::byte* ip;

 public:
        Chunk_istream(std::byte* chunk) : chunk{chunk}, ip{chunk} {}

        template<class T> Chunk_istream& operator>>(T& x)
        {
                std::copy_n(ip, sizeof(x), reinterpret_cast<std::byte*>(&x));
                ip += sizeof(x);
                return *this;
        }

        void seekg(size_t n) { ip = chunk + n; }
        size_t tellg() const { return ip - chunk; }
};

struct Frame {
        Chunk_istream code;
        val* fp;
        val* rp;
};

class Interp {
 private:
        State& s;
        std::vector<Frame> fs;
        bool verbose;
        val* stack_top;

        void jump(size_t addr)
        { fs.back().code.seekg(addr); }

        template<class T> void read(T& x)
        {
                fs.back().code >> x;
                if (verbose) {
                        if constexpr (std::integral<T>)
                                std::cout << " " << static_cast<long>(x);
                        else
                                std::cout << " " << x;
                }
        }

        val* read_val()
        {
                int16_t name;
                read(name);
                if (name < 0) return s.globals.data() + ~name;
                auto it = fs.back().fp + name;
                stack_top = std::max(stack_top, it + 1);
                return it;
        }

        void call()
        {
                auto return_addr = read_val();
                auto frame_start = stack_top;
                auto function = *read_val();
                if (s.is_closure(function)) {
                        auto [f,e] = get_closure(s, function);
                        auto r = get_tuple(s, e);
                        stack_top = std::copy(r.begin(), r.end(), stack_top);
                        function = f;
                }
                uint8_t num_args;
                read(num_args);
                auto first_arg = stack_top;
                for (size_t n = 0; n != num_args; ++n)
                        *stack_top++ = *read_val();
                auto last_arg = stack_top;
                if (s.is_sfun(function))
                        fs.emplace_back(get_sfun(s, function), frame_start, return_addr);
                else if (s.is_cfun(function))
                        *return_addr = get_cfun(s, function)(s,
                                        std::ranges::subrange(first_arg, last_arg));
                else
                        throw std::bad_cast{};
        }

        void instr()
        {
                Instr i;
                val *v1, *v2;
                uint8_t c1;
                uint16_t s1;

                if (verbose) {
                        auto flags = std::cout.flags();
                        std::cout << std::hex << std::setw(4) << fs.back().code.tellg() << ":";
                        std::cout.flags(flags);
                }
                read(i);
                switch (i) {
                 case Instr::cp:
                        v1 = read_val();
                        *read_val() = *v1;
                        break;

                 case Instr::neg:
                        v1 = read_val();
                        *read_val() = -*v1;
                        break;

                 case Instr::add:
                        v1 = read_val();
                        v2 = read_val();
                        *read_val() = concat(s, *v1, *v2);
                        break;

                 case Instr::sub:
                        v1 = read_val();
                        v2 = read_val();
                        *read_val() = *v1 - *v2;
                        break;

                 case Instr::mul:
                        v1 = read_val();
                        v2 = read_val();
                        *read_val() = *v1 * *v2;
                        break;

                 case Instr::div:
                        v1 = read_val();
                        v2 = read_val();
                        *read_val() = *v1 / *v2;
                        break;

                 case Instr::mod:
                        v1 = read_val();
                        v2 = read_val();
                        *read_val() = *v1 % *v2;
                        break;

                 case Instr::shl:
                        v1 = read_val();
                        v2 = read_val();
                        *read_val() = *v1 << *v2;
                        break;

                 case Instr::shr:
                        v1 = read_val();
                        v2 = read_val();
                        *read_val() = *v1 >> *v2;
                        break;

                 case Instr::binary_and:
                        v1 = read_val();
                        v2 = read_val();
                        *read_val() = *v1 & *v2;
                        break;

                 case Instr::binary_xor:
                        v1 = read_val();
                        v2 = read_val();
                        *read_val() = *v1 ^ *v2;
                        break;

                 case Instr::binary_or:
                        v1 = read_val();
                        v2 = read_val();
                        *read_val() = *v1 | *v2;
                        break;

                 case Instr::jmp:
                        read(s1);
                        jump(s1);
                        break;

                 case Instr::jt:
                        v1 = read_val();
                        read(s1);
                        if (is_true(*v1)) jump(s1);
                        break;

                 case Instr::jf:
                        v1 = read_val();
                        read(s1);
                        if (!is_true(*v1)) jump(s1);
                        break;

                 case Instr::jeq:
                        v1 = read_val();
                        v2 = read_val();
                        read(s1);
                        if (equal_to(s)(*v1, *v2)) jump(s1);
                        break;

                 case Instr::jne:
                        v1 = read_val();
                        v2 = read_val();
                        read(s1);
                        if (not_equal_to(s)(*v1, *v2)) jump(s1);
                        break;

                 case Instr::jlt:
                        v1 = read_val();
                        v2 = read_val();
                        read(s1);
                        if (less(s)(*v1, *v2)) jump(s1);
                        break;

                 case Instr::jle:
                        v1 = read_val();
                        v2 = read_val();
                        read(s1);
                        if (less_equal(s)(*v1, *v2)) jump(s1);
                        break;

                 case Instr::jgt:
                        v1 = read_val();
                        v2 = read_val();
                        read(s1);
                        if (greater(s)(*v1, *v2)) jump(s1);
                        break;

                 case Instr::jge:
                        v1 = read_val();
                        v2 = read_val();
                        read(s1);
                        if (greater_equal(s)(*v1, *v2)) jump(s1);
                        break;

                 case Instr::bind:
                        v1 = read_val();
                        v2 = read_val();
                        *v2 = s.bind(*v1, s());
                        get_tuple(s, *v2)[2] = s.mem(fs.back().fp, v2 + 1);
                        break;

                 case Instr::call:
                        call();
                        break;

                 case Instr::ret:
                        v1 = read_val();
                        stack_top = fs.back().fp;
                        *fs.back().rp = *v1;
                        fs.pop_back();
                        break;

                 case Instr::list:
                        read(c1);
                        {
                                std::vector<val> buf;
                                buf.reserve(c1);
                                while (c1--)
                                        buf.push_back(*read_val());
                                *read_val() = s(buf);
                        }
                        break;

                 case Instr::dict:
                        read(c1);
                        {
                                std::vector<pair> buf;
                                buf.reserve(c1);
                                while (c1--) {
                                        auto k = *read_val();
                                        auto v = *read_val();
                                        buf.emplace_back(k, v);
                                }
                                *read_val() = s(buf);
                        }
                        break;

                 case Instr::ldi:
                        v1 = read_val();
                        v2 = read_val();
                        *read_val() = access(s, *v1, *v2);
                        break;

                 case Instr::sti:
                        v1 = read_val();
                        v2 = read_val();
                        access(s, *v1, *v2) = *read_val();
                        break;

                 case Instr::begin:
                        v1 = read_val();
                        *read_val() = begin(s)(*v1);
                        break;

                 case Instr::end:
                        v1 = read_val();
                        *read_val() = end(s)(*v1);
                        break;

                 case Instr::deref:
                        v1 = read_val();
                        *read_val() = head(s, get_tuple(*v1));
                        break;

                 case Instr::adv:
                        advance(*read_val());
                        break;

                 default:
                        std::stringstream message;
                        message << "unrecognized instruction: " << i;
                        throw std::runtime_error{message.str()};
                }
                if (verbose) {
                        if (!fs.empty()) {
                                std::cout << "\e[25G| +" << (fs.back().fp - s.stack.data());
                                for (auto r : std::ranges::subrange(fs.back().fp, stack_top))
                                        std::cout << " " << colorize(to_string(s, r));
                        }
                        std::cout << "\n";
                }
        }

 public:
        Interp(State& s, bool verbose) : s{s}, verbose{verbose} {}

        val operator()(const val& entrypoint)
        {
                try {
                        s.stack.resize(1024);
                        stack_top = s.stack.data() + 1;
                        fs.emplace_back(get_sfun(s, entrypoint), stack_top, s.stack.data());
                        while (!fs.empty())
                                instr();
                        return s.stack[0];
                } catch (std::exception& ex) {
                        if (verbose)
                                std::cout << "\n";
                        throw;
                }
        }
};

val interp(State& s, const val& entrypoint, bool verbose)
{ return Interp{s, verbose}(entrypoint); }

}; // end namespace shon

