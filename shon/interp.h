
#ifndef SHON_INTERP_H_
#define SHON_INTERP_H_

#include <vector>
#include <iostream>
#include <iomanip>

#include "state.h"

namespace shon {

val interp(State& s, const val& entrypoint, bool verbose);

}; // namespace shon

#endif // SHON_INTERP_H_
