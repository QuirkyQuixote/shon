%token INDENT DEDENT NEXT BAD_INDENT
%token IDENTIFIER NUMBER STRING
%token PTR_OP INC_OP DEC_OP LEFT_OP RIGHT_OP LE_OP GE_OP EQ_OP NE_OP
%token AND_OP OR_OP MUL_ASSIGN DIV_ASSIGN MOD_ASSIGN ADD_ASSIGN
%token SUB_ASSIGN SHL_ASSIGN SHR_ASSIGN AND_ASSIGN
%token XOR_ASSIGN OR_ASSIGN

%token IF ELSE ELIF WHILE DO FOR IN CONTINUE BREAK RETURN FUN IMPORT

%start program

%lex-param {shon::Parser_context& p}
%parse-param {shon::Parser_context& p}

%define parse.error detailed
%define api.value.type {shon::ast::val}
%locations
%define api.pure full

%code requires {
#include <vector>
#include <string>
#include <charconv>
#include <ostream>

#include "tree.h"

namespace shon {

namespace ast {

enum class tag {
        identifier, nil, number, string, list, statement_list, if_statement,
        while_statement, for_statement, continue_statement, break_statement,
        return_statement, empty_statement, comma_expr, assign_expr, lnot_expr,
        land_expr, lor_expr, bnot_expr, band_expr, bor_expr, bxor_expr,
        eq_expr, ne_expr, lt_expr, gt_expr, le_expr, ge_expr, inc_expr,
        dec_expr, pos_expr, neg_expr, add_expr, sub_expr, mul_expr, div_expr,
        mod_expr, shl_expr, shr_expr, idx_expr, fwd_expr, list_expr, dict_expr,
        pair_expr, call_expr, call_argument_list, tagged_argument_list,
        function, function_argument_list, declaration_list, declaration,
        import, scope, end, count
};

inline std::ostream& operator<<(std::ostream& os, tag t)
{
        static constexpr const char* names_[] = {
                "identifier", "nil", "number", "string", "list",
                "statement_list", "if_statement", "while_statement",
                "for_statement", "continue_statement", "break_statement",
                "return_statement", "empty_statement", "comma_expr",
                "assign_expr", "lnot_expr", "land_expr", "lor_expr",
                "bnot_expr", "band_expr", "bor_expr", "bxor_expr", "eq_expr",
                "ne_expr", "lt_expr", "gt_expr", "le_expr", "ge_expr",
                "inc_expr", "dec_expr", "pos_expr", "neg_expr", "add_expr",
                "sub_expr", "mul_expr", "div_expr", "mod_expr", "shl_expr",
                "shr_expr", "idx_expr", "fwd_expr", "list_expr", "dict_expr",
                "pair_expr", "call_expr", "call_argument_list",
                "tagged_argument_list", "function", "function_argument_list",
                "declaration_list", "declaration", "import", "scope", "end"
        };
        if (t < tag::count)
                return os << names_[static_cast<size_t>(t)];
        return os << static_cast<size_t>(t);
};

using pool = Tree<tag>;
using val = Val<tag>;

}; // namespace ast

struct Parser_context {
        ast::pool pool;
        ast::val root;
        std::string indent_cur;
        std::vector<size_t> indent_st{0};
        int indent_tgt = 0;

        int indent(void);
        int do_indent(void);
};

std::pair<ast::pool, ast::val> parse();

}; // namespace shon

void yyrestart(FILE*);

} // code requires

%code provides{
extern char* yytext;
extern int yyleng;

int yylex(YYSTYPE* yylvalp, YYLTYPE* yyllocp, shon::Parser_context& p);
void yyerror(YYLTYPE* yylocp, shon::Parser_context& p, const char* message);
}

%%

identifier
        : IDENTIFIER
        { $$ = p.pool(p.pool(shon::ast::tag::identifier), p.pool(yytext)); }
        ;

nil
        : '\''
        { $$ = p.pool(p.pool(shon::ast::tag::nil)); }
        ;
number
        : NUMBER {
                int n;
                std::from_chars(yytext, yytext + yyleng, n);
                $$ = p.pool(p.pool(shon::ast::tag::number), p.pool(n));
        }
        ;

string
        : STRING {
                std::string s = shon::escape(std::string_view(yytext + 1, yyleng - 2));
                $$ = p.pool(p.pool(shon::ast::tag::string), p.pool(s));
        }
        ;

string_from_ident
        : IDENTIFIER {
                $$ = p.pool(p.pool(shon::ast::tag::string), p.pool(yytext));
        }
        ;

primary_expression
	: identifier
        | nil
        | number
        | string
	| '(' expression ')' { $$ = $2; }
        | '[' ']' {
                auto args = p.pool(p.pool(shon::ast::tag::call_argument_list));
                $$ = p.pool(p.pool(shon::ast::tag::list_expr), args);
        }
        | '[' argument_expression_list ']' {
                auto args = p.pool(p.pool(shon::ast::tag::call_argument_list), $2);
                $$ = p.pool(p.pool(shon::ast::tag::list_expr), args);
        }
        | '{' '}' {
                auto args = p.pool(p.pool(shon::ast::tag::tagged_argument_list));
                $$ = p.pool(p.pool(shon::ast::tag::dict_expr), args);
        }
        | '{' tagged_expression_list '}' {
                auto args = p.pool(p.pool(shon::ast::tag::tagged_argument_list), $2);
                $$ = p.pool(p.pool(shon::ast::tag::dict_expr), args);
        }
	;

postfix_expression
	: primary_expression
	| postfix_expression '[' expression ']'
        { $$ = p.pool(p.pool(shon::ast::tag::idx_expr), $1, $3); }
        | postfix_expression '.' string_from_ident
        { $$ = p.pool(p.pool(shon::ast::tag::idx_expr), $1, $3); }
	| postfix_expression '(' ')' {
                auto args = p.pool(p.pool(shon::ast::tag::call_argument_list));
                $$ = p.pool(p.pool(shon::ast::tag::call_expr), $1, args);
        }
	| postfix_expression '(' argument_expression_list ')' {
                auto args = p.pool(p.pool(shon::ast::tag::call_argument_list), $3);
                $$ = p.pool(p.pool(shon::ast::tag::call_expr), $1, args);
        }
	| postfix_expression INC_OP
	| postfix_expression DEC_OP
	;

tagged_expression
        : string_from_ident ':' expression
        { $$ = p.pool(p.pool(shon::ast::tag::pair_expr), $1, $3); }
        | number ':' expression
        { $$ = p.pool(p.pool(shon::ast::tag::pair_expr), $1, $3); }
        | string ':' expression
        { $$ = p.pool(p.pool(shon::ast::tag::pair_expr), $1, $3); }
        ;

tagged_expression_list
        : tagged_expression
        | tagged_expression_list ',' tagged_expression
        { $$ = p.pool(p.pool(shon::ast::tag::list), $1, $3); }
	;

argument_expression_list
	: assignment_expression
	| argument_expression_list ',' assignment_expression
        { $$ = p.pool(p.pool(shon::ast::tag::list), $1, $3); }
	;

unary_expression
	: postfix_expression
	| INC_OP unary_expression
        { $$ = p.pool(p.pool(shon::ast::tag::inc_expr), $2); }
	| DEC_OP unary_expression
        { $$ = p.pool(p.pool(shon::ast::tag::dec_expr), $2); }
	| '+' unary_expression
        { $$ = p.pool(p.pool(shon::ast::tag::pos_expr), $2); }
	| '-' unary_expression
        { $$ = p.pool(p.pool(shon::ast::tag::neg_expr), $2); }
	| '~' unary_expression
        { $$ = p.pool(p.pool(shon::ast::tag::bnot_expr), $2); }
	| '!' unary_expression
        { $$ = p.pool(p.pool(shon::ast::tag::lnot_expr), $2); }
	;

multiplicative_expression
	: unary_expression
	| multiplicative_expression '*' unary_expression
        { $$ = p.pool(p.pool(shon::ast::tag::mul_expr), $1, $3); }
	| multiplicative_expression '/' unary_expression
        { $$ = p.pool(p.pool(shon::ast::tag::div_expr), $1, $3); }
	| multiplicative_expression '%' unary_expression
        { $$ = p.pool(p.pool(shon::ast::tag::mod_expr), $1, $3); }
	;

additive_expression
	: multiplicative_expression
	| additive_expression '+' multiplicative_expression
        { $$ = p.pool(p.pool(shon::ast::tag::add_expr), $1, $3); }
	| additive_expression '-' multiplicative_expression
        { $$ = p.pool(p.pool(shon::ast::tag::sub_expr), $1, $3); }
	;

shift_expression
	: additive_expression
	| shift_expression LEFT_OP additive_expression
        { $$ = p.pool(p.pool(shon::ast::tag::shl_expr), $1, $3); }
	| shift_expression RIGHT_OP additive_expression
        { $$ = p.pool(p.pool(shon::ast::tag::shr_expr), $1, $3); }
	;

relational_expression
	: shift_expression
	| relational_expression '<' shift_expression
        { $$ = p.pool(p.pool(shon::ast::tag::lt_expr), $1, $3); }
	| relational_expression '>' shift_expression
        { $$ = p.pool(p.pool(shon::ast::tag::gt_expr), $1, $3); }
	| relational_expression LE_OP shift_expression
        { $$ = p.pool(p.pool(shon::ast::tag::le_expr), $1, $3); }
	| relational_expression GE_OP shift_expression
        { $$ = p.pool(p.pool(shon::ast::tag::ge_expr), $1, $3); }
	;

equality_expression
	: relational_expression
	| equality_expression EQ_OP relational_expression
        { $$ = p.pool(p.pool(shon::ast::tag::eq_expr), $1, $3); }
	| equality_expression NE_OP relational_expression
        { $$ = p.pool(p.pool(shon::ast::tag::ne_expr), $1, $3); }
	;

and_expression
	: equality_expression
	| and_expression '&' equality_expression
        { $$ = p.pool(p.pool(shon::ast::tag::band_expr), $1, $3); }
	;

exclusive_or_expression
	: and_expression
	| exclusive_or_expression '^' and_expression
        { $$ = p.pool(p.pool(shon::ast::tag::bxor_expr), $1, $3); }
	;

inclusive_or_expression
	: exclusive_or_expression
	| inclusive_or_expression '|' exclusive_or_expression
        { $$ = p.pool(p.pool(shon::ast::tag::bor_expr), $1, $3); }
	;

logical_and_expression
	: inclusive_or_expression
	| logical_and_expression AND_OP inclusive_or_expression
        { $$ = p.pool(p.pool(shon::ast::tag::land_expr), $1, $3); }
	;

logical_or_expression
	: logical_and_expression
	| logical_or_expression OR_OP logical_and_expression
        { $$ = p.pool(p.pool(shon::ast::tag::lor_expr), $1, $3); }
	;

conditional_expression
	: logical_or_expression
	| logical_or_expression '?' expression ':' conditional_expression
	;

assignment_expression
	: conditional_expression
	| unary_expression '=' assignment_expression
        { $$ = p.pool(p.pool(shon::ast::tag::assign_expr), $1, $3); }
	| unary_expression MUL_ASSIGN assignment_expression {
                auto r = p.pool(p.pool(shon::ast::tag::mul_expr), $1, $3);
                $$ = p.pool(p.pool(shon::ast::tag::assign_expr), $1, r);
        }
	| unary_expression DIV_ASSIGN assignment_expression {
                auto r = p.pool(p.pool(shon::ast::tag::div_expr), $1, $3);
                $$ = p.pool(p.pool(shon::ast::tag::assign_expr), $1, r);
        }
	| unary_expression MOD_ASSIGN assignment_expression {
                auto r = p.pool(p.pool(shon::ast::tag::mod_expr), $1, $3);
                $$ = p.pool(p.pool(shon::ast::tag::assign_expr), $1, r);
        }
	| unary_expression ADD_ASSIGN assignment_expression {
                auto r = p.pool(p.pool(shon::ast::tag::add_expr), $1, $3);
                $$ = p.pool(p.pool(shon::ast::tag::assign_expr), $1, r);
        }
	| unary_expression SUB_ASSIGN assignment_expression {
                auto r = p.pool(p.pool(shon::ast::tag::sub_expr), $1, $3);
                $$ = p.pool(p.pool(shon::ast::tag::assign_expr), $1, r);
        }
	| unary_expression SHL_ASSIGN assignment_expression {
                auto r = p.pool(p.pool(shon::ast::tag::shl_expr), $1, $3);
                $$ = p.pool(p.pool(shon::ast::tag::assign_expr), $1, r);
        }
	| unary_expression SHR_ASSIGN assignment_expression {
                auto r = p.pool(p.pool(shon::ast::tag::shr_expr), $1, $3);
                $$ = p.pool(p.pool(shon::ast::tag::assign_expr), $1, r);
        }
	| unary_expression AND_ASSIGN assignment_expression {
                auto r = p.pool(p.pool(shon::ast::tag::band_expr), $1, $3);
                $$ = p.pool(p.pool(shon::ast::tag::assign_expr), $1, r);
        }
	| unary_expression OR_ASSIGN assignment_expression {
                auto r = p.pool(p.pool(shon::ast::tag::bor_expr), $1, $3);
                $$ = p.pool(p.pool(shon::ast::tag::assign_expr), $1, r);
        }
	| unary_expression XOR_ASSIGN assignment_expression {
                auto r = p.pool(p.pool(shon::ast::tag::bxor_expr), $1, $3);
                $$ = p.pool(p.pool(shon::ast::tag::assign_expr), $1, r);
        }
	;

expression
	: assignment_expression
	;

expression_statement
	: expression next
	;

else_statement
        : ELIF expression ':' next scope else_statement
        { $$ = p.pool(p.pool(shon::ast::tag::if_statement), $2, $5, $6); }
        | ELSE ':' next scope
        { $$ = $4; }
        |
        { $$ = p.pool(p.pool(shon::ast::tag::empty_statement)); }
        ;

selection_statement
	: IF expression ':' next scope else_statement
        { $$ = p.pool(p.pool(shon::ast::tag::if_statement), $2, $5, $6); }
	;

iteration_statement
	: WHILE expression ':' next scope
        { $$ = p.pool(p.pool(shon::ast::tag::while_statement), $2, $5); }
	| FOR expression IN expression ':' next scope
        { $$ = p.pool(p.pool(shon::ast::tag::for_statement), $2, $4, $7); }
	;

jump_statement
	: CONTINUE next
        { $$ = p.pool(p.pool(shon::ast::tag::continue_statement)); }
	| BREAK next
        { $$ = p.pool(p.pool(shon::ast::tag::break_statement)); }
	| RETURN next
        { $$ = p.pool(p.pool(shon::ast::tag::return_statement)); }
	| RETURN expression next
        { $$ = p.pool(p.pool(shon::ast::tag::return_statement), $2); }
	;

declaration_statement
        : FUN identifier '(' ')' ':' next scope {
                auto args = p.pool(p.pool(shon::ast::tag::function_argument_list));
                $$ = p.pool(p.pool(shon::ast::tag::function), $2, args, $7);
        }
        | FUN identifier '(' identifier_list ')' ':' next scope {
                auto args = p.pool(p.pool(shon::ast::tag::function_argument_list), $4);
                $$ = p.pool(p.pool(shon::ast::tag::function), $2, args, $8);
        }
        | IMPORT identifier next {
                auto string = get_tuple(p.pool, $2)[1];
                $$ = p.pool(p.pool(shon::ast::tag::import), $2, string);
        }
        ;

statement
	: expression_statement
	| selection_statement
	| iteration_statement
	| jump_statement
        | declaration_statement
	;

statement_list
	: statement
	| statement_list statement
        { $$ = p.pool(p.pool(shon::ast::tag::list), $1, $2); }
	;

dedent
        : DEDENT
        | YYEOF
        ;

next
        : NEXT
        | YYEOF
        ;

scope
        : INDENT statement_list dedent
        { $$ = p.pool(p.pool(shon::ast::tag::statement_list), $2); }

identifier_list
        : identifier
        | identifier_list ',' identifier
        { $$ = p.pool(p.pool(shon::ast::tag::list), $1, $3); }
        ;

discard_next
        : NEXT
        |
        ;

program
        : discard_next statement_list 
        { p.root = p.pool(p.pool(shon::ast::tag::statement_list), $2); }

%%
#include <iostream>

void yyerror(YYLTYPE* yylocp, shon::Parser_context& p, const char *message)
{
        std::cerr << "\e[0;31m" << yylocp->first_line << "," <<
                yylocp->first_column << ": " << message << "\e[0;0m\n";
}

namespace shon {

std::pair<ast::pool, ast::val> parse()
{
        Parser_context p;
        yyparse(p);
        return std::make_pair(std::move(p.pool), p.root);
}

}; // namespace shon
