
#include "prune.h"

#include <stdexcept>
#include <sstream>

namespace shon {

class Pruner {
 private:
        using tuple_type = std::ranges::subrange<const ast::val*>;

        const ast::pool& in_;
        Tagged_ast c_;
        std::vector<std::string> symbols_;
        std::vector<size_t> scope_stack_;

        tuple_type bang_(ast::val a) { return get_tuple(in_, a); }

        void push_scope_()
        {
                scope_stack_.push_back(symbols_.size());
        }

        void pop_scope_()
        {
                symbols_.resize(scope_stack_.back());
                scope_stack_.pop_back();
        }

        ast::val list_(tuple_type t, ast::tag tag, ast::val(Pruner::*func)(tuple_type))
        {
                std::vector<tuple_type> stack;
                for (;;) {
                        if (get_tag(t[0]) != ast::tag::list)
                                break;
                        stack.push_back(bang_(t[2]));
                        t = bang_(t[1]);
                }
                stack.push_back(t);
                std::vector<ast::val> solution{c_.pool(tag)};
                while (!stack.empty()) {
                        solution.push_back((this->*func)(stack.back()));
                        stack.pop_back();
                }
                return c_.pool(solution);
        }

        ast::val list_or_empty_(tuple_type t, ast::tag tag, ast::val(Pruner::*func)(tuple_type))
        {
                if (t.size() == 1) return c_.pool(c_.pool(tag));
                return list_(bang_(t[1]), tag, func);
        }

        ast::val invisible_identifier_()
        {
                auto n = symbols_.size();
                symbols_.emplace_back("");
                return c_.pool(c_.pool(ast::tag::identifier), c_.pool(n));
        }

        ast::val identifier_(tuple_type t)
        {
                auto s = get_string(in_, t[1]);
                auto i = std::ranges::find(symbols_, s);
                if (i == symbols_.end())
                        i = symbols_.emplace(i, s);
                return c_.pool(c_.pool(ast::tag::identifier), c_.pool(i - symbols_.begin()));
        }

        ast::val nil_(tuple_type t)
        {
                return c_.pool(c_.pool(ast::tag::nil));
        }

        ast::val number_(tuple_type t)
        {
                auto c1 = c_.pool(get_number(t[1]));
                return c_.pool(c_.pool(ast::tag::number), c1);
        }

        ast::val string_(tuple_type t)
        {
                auto c1 = c_.pool(get_string(in_, t[1]));
                return c_.pool(c_.pool(ast::tag::string), c1);
        }

        ast::val binary_op_(tuple_type t)
        {
                auto c1 = expression_(bang_(t[1]));
                auto c2 = expression_(bang_(t[2]));
                return c_.pool(c_.pool(get_tag(t[0])), c1, c2);
        }

        ast::val unary_op_(tuple_type t)
        {
                auto c1 = expression_(bang_(t[1]));
                return c_.pool(c_.pool(get_tag(t[0])), c1);
        }

        ast::val tagged_argument_(tuple_type t)
        {
                auto c1 = expression_(bang_(t[1]));
                auto c2 = expression_(bang_(t[2]));
                return c_.pool(c_.pool(ast::tag::pair_expr), c1, c2);
        }

        ast::val tagged_argument_list_(tuple_type t)
        {
                return list_or_empty_(t, ast::tag::tagged_argument_list,
                                &Pruner::tagged_argument_);
        }

        ast::val dict_expr_(tuple_type t)
        {
                auto c1 = tagged_argument_list_(bang_(t[1]));
                return c_.pool(c_.pool(ast::tag::dict_expr), c1);
        }

        ast::val call_argument_(tuple_type t)
        {
                return expression_(t);
        }

        ast::val call_argument_list_(tuple_type t)
        {
                return list_or_empty_(t, ast::tag::call_argument_list,
                                &Pruner::call_argument_);
        }

        ast::val list_expr_(tuple_type t)
        {
                auto c1 = call_argument_list_(bang_(t[1]));
                return c_.pool(c_.pool(ast::tag::list_expr), c1);
        }

        ast::val call_expr_(tuple_type t)
        {
                auto c1 = expression_(bang_(t[1]));
                auto c2 = call_argument_list_(bang_(t[2]));
                return c_.pool(c_.pool(ast::tag::call_expr), c1, c2);
        }

        ast::val logical_or_(tuple_type t)
        {
                auto c1 = expression_(bang_(t[1]));
                auto c2 = expression_(bang_(t[2]));
                return c_.pool(c_.pool(ast::tag::lnot_expr),
                        c_.pool(c_.pool(ast::tag::land_expr),
                                c_.pool(c_.pool(ast::tag::lnot_expr), c1),
                                c_.pool(c_.pool(ast::tag::lnot_expr), c2)));
        }

        ast::val expression_(tuple_type t)
        {
                switch (get_tag(t[0])) {
                 case ast::tag::identifier: return identifier_(t);
                 case ast::tag::nil: return nil_(t);
                 case ast::tag::number: return number_(t);
                 case ast::tag::string: return string_(t);

                 case ast::tag::eq_expr:
                 case ast::tag::ne_expr:
                 case ast::tag::lt_expr:
                 case ast::tag::gt_expr:
                 case ast::tag::le_expr:
                 case ast::tag::ge_expr:

                 case ast::tag::mul_expr:
                 case ast::tag::div_expr:
                 case ast::tag::mod_expr:
                 case ast::tag::add_expr:
                 case ast::tag::sub_expr:
                 case ast::tag::shl_expr:
                 case ast::tag::shr_expr:
                 case ast::tag::band_expr:
                 case ast::tag::bxor_expr:
                 case ast::tag::bor_expr:

                 case ast::tag::assign_expr:
                 case ast::tag::land_expr:
                 case ast::tag::idx_expr: return binary_op_(t);

                 case ast::tag::lor_expr: return logical_or_(t);

                 case ast::tag::bnot_expr:
                 case ast::tag::lnot_expr:
                 case ast::tag::neg_expr:
                 case ast::tag::pos_expr: return unary_op_(t);

                 case ast::tag::list_expr: return list_expr_(t);

                 case ast::tag::dict_expr: return dict_expr_(t);

                 case ast::tag::call_expr: return call_expr_(t);

                 default: break;
                }
                std::stringstream message;
                message << "expected expression, got " << get_tag(t[0]) <<
                        " instead";
                throw std::runtime_error{message.str()};
        }

        ast::val for_statement_(tuple_type t)
        {
                push_scope_();
                auto c1 = invisible_identifier_();
                auto c2 = invisible_identifier_();
                auto c3 = expression_(bang_(t[1]));
                auto c4 = expression_(bang_(t[2]));
                auto c5 = statement_list_(bang_(t[3]));
                auto r = c_.pool(c_.pool(ast::tag::for_statement), c1, c2, c3, c4, c5);
                pop_scope_();
                return r;
        }

        ast::val while_statement_(tuple_type t)
        {
                push_scope_();
                auto c1 = expression_(bang_(t[1]));
                auto c2 = statement_list_(bang_(t[2]));
                auto r = c_.pool(c_.pool(ast::tag::while_statement), c1, c2);
                pop_scope_();
                return r;
        }

        ast::val else_statement_(tuple_type t)
        {
                switch (get_tag(t[0])) {
                 case ast::tag::if_statement: return if_statement_(t);
                 case ast::tag::statement_list: return statement_list_(t);
                 case ast::tag::empty_statement: return c_.pool(c_.pool(ast::tag::empty_statement));
                 default: break;
                }
                std::stringstream message;
                message << "expected else statement, got " <<
                        get_tag(t[0]) << " instead";
                throw std::runtime_error{message.str()};
        }

        ast::val if_statement_(tuple_type t)
        {
                push_scope_();
                auto c1 = expression_(bang_(t[1]));
                auto c2 = statement_list_(bang_(t[2]));
                auto c3 = else_statement_(bang_(t[3]));
                auto r = c_.pool(c_.pool(ast::tag::if_statement), c1, c2, c3);
                pop_scope_();
                return r;
        }

        ast::val continue_statement_(tuple_type t)
        {
                return c_.pool(c_.pool(ast::tag::continue_statement));
        }

        ast::val break_statement_(tuple_type t)
        {
                return c_.pool(c_.pool(ast::tag::break_statement));
        }

        ast::val return_statement_(tuple_type t)
        {
                auto c1 = expression_(bang_(t[1]));
                return c_.pool(c_.pool(ast::tag::return_statement), c1);
        }

        ast::val import_statement_(tuple_type t)
        {
                auto i = identifier_(bang_(t[1]));
                auto s = c_.pool(get_string(in_, t[2]));
                return c_.pool(c_.pool(ast::tag::import), i, s);
        }

        ast::val statement_list_(tuple_type t)
        {
                return list_(bang_(t[1]), ast::tag::statement_list,
                                &Pruner::statement_);
        }

        ast::val function_argument_(tuple_type t)
        {
                return identifier_(t);
        }

        ast::val function_argument_list_(tuple_type t)
        {
                return list_or_empty_(t, ast::tag::function_argument_list,
                                &Pruner::function_argument_);
        }

        ast::val function_statement_(tuple_type t)
        {
                push_scope_();
                auto c1 = identifier_(bang_(t[1]));
                auto c2 = function_argument_list_(bang_(t[2]));
                auto c3 = statement_list_(bang_(t[3]));
                auto r = c_.pool(c_.pool(ast::tag::function), c1, c2, c3);
                pop_scope_();
                return r;
        }

        ast::val statement_(tuple_type t)
        {
                switch (get_tag(t[0])) {
                 case ast::tag::return_statement: return return_statement_(t);
                 case ast::tag::break_statement: return break_statement_(t);
                 case ast::tag::continue_statement: return continue_statement_(t);
                 case ast::tag::for_statement: return for_statement_(t);
                 case ast::tag::while_statement: return while_statement_(t);
                 case ast::tag::if_statement: return if_statement_(t);
                 case ast::tag::import: return import_statement_(t);
                 case ast::tag::function: return function_statement_(t);
                 default: return expression_(t);
                }
        }

 public:

        Pruner(const ast::pool& in) : in_{in} {}

        Tagged_ast operator()(const ast::val& root)
        {
                c_.root = statement_list_(bang_(root));
                return std::move(c_);
        }
};

Tagged_ast prune(const ast::pool& in, const ast::val& root)
{ return Pruner{in}(root); }

}; // namespace shon
