
#ifndef SHON_PRUNE_H_
#define SHON_PRUNE_H_

#include "parser.hh"

namespace shon {

struct Tagged_ast {
        ast::pool pool;
        ast::val root;
};

Tagged_ast prune(const ast::pool& p, const ast::val& root);

}; // namespace shon

#endif // SHON_PRUNE_H_
