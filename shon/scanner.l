D			[0-9]
L			[a-zA-Z_]
H			[a-fA-F0-9]
E			[Ee][+-]?{D}+
FS			(f|F|l|L)
IS			(u|U|l|L)*

%x indentation

%{
#include <iostream>

#include "parser.hh"

#define YY_DECL int yylex(shon::ast::val* yylvalp, YYLTYPE* yyllocp, shon::Parser_context& p)
#define YY_NO_UNPUT

void count(YYLTYPE* l)
{
        l->first_line = l->last_line;
        l->first_column = l->last_column;
        for (auto p = yytext; *p; ++p) {
		if (*p == '\n') {
                        l->last_column = 1;
                        ++l->last_line;
                } else if (*p == '\t') {
                        l->last_column += 8 - (l->last_column % 8);
                } else {
                        l->last_column++;
                }
        }
}

%}

%%
"elif"			{ count(yyllocp); return ELIF; }
"else"			{ count(yyllocp); return ELSE; }
"for"			{ count(yyllocp); return FOR; }
"fun"			{ count(yyllocp); return FUN; }
"import"		{ count(yyllocp); return IMPORT; }
"in"                    { count(yyllocp); return IN; }
"if"			{ count(yyllocp); return IF; }
"return"		{ count(yyllocp); return RETURN; }
"while"			{ count(yyllocp); return WHILE; }

{L}({L}|{D})*		{ count(yyllocp); return IDENTIFIER; }

0[xX]{H}+{IS}?		{ count(yyllocp); return NUMBER; }
0{D}+{IS}?		{ count(yyllocp); return NUMBER; }
{D}+{IS}?		{ count(yyllocp); return NUMBER; }

L?\"(\\.|[^\\"])*\"	{ count(yyllocp); return STRING; }

">>="			{ count(yyllocp); return SHR_ASSIGN; }
"<<="			{ count(yyllocp); return SHL_ASSIGN; }
"+="			{ count(yyllocp); return ADD_ASSIGN; }
"-="			{ count(yyllocp); return SUB_ASSIGN; }
"*="			{ count(yyllocp); return MUL_ASSIGN; }
"/="			{ count(yyllocp); return DIV_ASSIGN; }
"%="			{ count(yyllocp); return MOD_ASSIGN; }
"&="			{ count(yyllocp); return AND_ASSIGN; }
"^="			{ count(yyllocp); return XOR_ASSIGN; }
"|="			{ count(yyllocp); return OR_ASSIGN; }
">>"			{ count(yyllocp); return RIGHT_OP; }
"<<"			{ count(yyllocp); return LEFT_OP; }
"++"			{ count(yyllocp); return INC_OP; }
"--"			{ count(yyllocp); return DEC_OP; }
"->"			{ count(yyllocp); return PTR_OP; }
"&&"			{ count(yyllocp); return AND_OP; }
"||"			{ count(yyllocp); return OR_OP; }
"<="			{ count(yyllocp); return LE_OP; }
">="			{ count(yyllocp); return GE_OP; }
"=="			{ count(yyllocp); return EQ_OP; }
"!="			{ count(yyllocp); return NE_OP; }
(\n[ \t]*)+             { count(yyllocp); return p.indent(); }
[ \t]*                  { count(yyllocp); }
<indentation>[ \t\n]+   { return p.do_indent(); }
.			{ count(yyllocp); return yytext[0]; }

%%

int yywrap(void)
{
	return 1;
}

namespace shon {

// Find first mismatch character between current indentation and yytext

// If yytext is shorter than the current, rewind the stack until we find the
// desired indentation level. If we end between stack points, the indentation
// is bad.

// If yytext is longer than the current, push a new indentation level to the
// stack.

int Parser_context::indent()
{
        auto start = strrchr(yytext, '\n') + 1;
        auto [p1, p2] = std::mismatch(indent_cur.begin(), indent_cur.end(),
                                      start, yytext + yyleng);

        if (*p1 != 0) {
                while (p1 < indent_cur.begin() + indent_st.back()) {
                        indent_st.pop_back();
                        if (p1 > indent_cur.begin() + indent_st.back()) {
                                std::cerr << "Bad indentation:\n";
                                std::cerr << "current: \"" << indent_cur << "\"\n";
                                std::cerr << "yytext: \"" << start << "\"\n";
                                return BAD_INDENT;
                        }
                        --indent_tgt;
                }
                indent_cur.resize(indent_st.back());
                yyless(0);
                BEGIN(indentation);
        } else if (*p2 != 0) {
                indent_cur = start;
                indent_st.push_back(indent_cur.size());
                ++indent_tgt;
                yyless(0);
                BEGIN(indentation);
        }
        return NEXT;
}

int Parser_context::do_indent()
{
        if (indent_tgt > 0) {
                if (--indent_tgt == 0) BEGIN(INITIAL);
                else yyless(0);
                return INDENT;
        }
        if (indent_tgt < 0) {
                if (++indent_tgt == 0) BEGIN(INITIAL);
                else yyless(0);
                return DEDENT;
        }
        std::cerr << "do_indent() called with indent_tgt == 0\n";
        return BAD_INDENT;
}

}; // namespace shon
