
#ifndef SHON_H_
#define SHON_H_

#include <cstring>

#include <filesystem>
#include <ranges>

#include "parser.hh"
#include "prune.h"
#include "compiler.h"
#include "interp.h"
#include "color.h"

namespace shon {

struct Config {
        bool verbose_parser = false;
        bool verbose_pruner = false;
        bool verbose_flattener = false;
        bool verbose_compiler = false;
        bool verbose_interpreter = false;
};

static Config config;

static const char* separator = "================================================";

void print_globals(std::ostream& os, const Flat_ast& s)
{
#if 0
        auto f = os.flags();
        auto& keys = s.globals;
        auto& vals = s.stack;
        for (size_t i = 0; i != keys.size(); ++i) {
                os << std::setw(16) << std::left << keys[i] << "= " <<
                        shon::colorize(to_string(s.pool, vals[i])) << "\n";
        }
        os.flags(f);
#endif
}

void print_code(std::ostream& os, const State& c)
{
#if 0
        os << "\e[0;34m";
        for (auto i : c.code)
                os << std::hex << std::setw(2) << std::setfill('0') <<
                        static_cast<int>(static_cast<uint8_t>(i)) << " ";
        os << std::dec << "\e[0;0m\n";
#endif
}

void load(State& state)
{
        auto [ast, root] = parse();
        if (config.verbose_parser) {
                std::cout << "Original AST\n";
                std::cout << colorize(to_string(ast, root)) << "\n";
                std::cout << separator << "\n";
        }

        auto tagged = prune(ast, root);
        if (config.verbose_pruner) {
                std::cout << "Tagged AST\n";
                std::cout << colorize(to_string(tagged.pool, tagged.root)) << "\n";
                std::cout << separator << "\n";
        }

        auto flat = flatten(tagged, state);
        if (config.verbose_flattener) {
                std::cout << "Flat AST\n";
                for (auto r : flat.roots)
                        std::cout << colorize(to_string(flat, r)) << "\n";
                std::cout << separator << "\n";
        }

        compile(flat, state);
        if (config.verbose_compiler) {
                std::cout << "Globals\n";
                for (auto r : state.globals)
                        std::cout << colorize(to_string(state, r)) << "\n";
                std::cout << separator << "\n";
        }
}

void load(State& state, const std::filesystem::path& path)
{
        FILE* stream = fopen(path.c_str(), "r");
        if (stream == nullptr) {
                std::stringstream message;
                message << path << ": " << strerror(errno);
                throw std::runtime_error{message.str()};
        }
        yyrestart(stream);
        load(state);
        fclose(stream);
}

val run(State& state, const val& entrypoint)
{
        if (config.verbose_interpreter) std::cout << "Execution Log\n";
        auto ret = interp(state, entrypoint, config.verbose_interpreter);
        if (config.verbose_interpreter) std::cout << separator << "\n";
        return ret;
}

}; // namespace shon

#endif // SHON_H_
