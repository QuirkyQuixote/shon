
#ifndef SHON_VAL_H_
#define SHON_VAL_H_

#include <ostream>

#include "tree.h"

namespace shon {

enum class data_tag { nil_, false_, true_, list_, dict_, sfun_, cfun_, bind_, count_ };

inline std::ostream& operator<<(std::ostream& os, data_tag t)
{
        static const char* names[] = {
                "'", "F", "T", "list", "dict", "sfun", "cfun", "bind"
        };
        if (t < data_tag::count_)
                return os << names[static_cast<size_t>(t)];
        return os << static_cast<size_t>(t);
}

enum class Instr : uint8_t {
        cp, neg, add, sub, mul, div, mod, shl, shr, binary_not, binary_and,
        binary_xor, binary_or, jmp, jt, jf, jeq, jne, jlt, jle, jgt, jge,
        call, ret, list, dict, begin, end, adv, deref, ldi, sti, bind, count
};

inline std::ostream& operator<<(std::ostream& os, Instr i)
{
        static const char* names[] = {
                "cp", "neg", "add", "sub", "mul", "div", "mod", "shl", "shr",
                "not", "and", "xor", "or", "jmp", "jt", "jf", "jeq", "jne",
                "jlt", "jle", "jgt", "jge", "call", "ret", "list", "dict",
                "begin", "end", "adv", "deref", "ldi", "sti", "bind"
        };
        if (i < Instr::count)
                return os << names[static_cast<size_t>(i)];
        return os << static_cast<size_t>(i);
}

using pool = Tree<data_tag>;
using val = Val<data_tag>;
using ref = Ref<val>;
using pair = std::pair<val, val>;
using tuple = std::ranges::subrange<val*>;
using const_tuple = std::ranges::subrange<const val*>;

struct State;

template<class T> T next_power_of_two(T n)
{
        T r = 1;
        while (r < n)
                r *= 2;
        return r;
}

struct compare_three_way {
        const State& s;
        compare_three_way(const State& s) : s{s} {}
        std::partial_ordering operator()(const val& l, const val& r) const;
};

struct equal_to {
        compare_three_way compare;
        equal_to(const State& s) : compare{s} {}
        bool operator()(const val& l, const val& r) const
        { return compare(l, r) == 0; }
};

struct not_equal_to {
        compare_three_way compare;
        not_equal_to(const State& s) : compare{s} {}
        bool operator()(const val& l, const val& r) const
        { return compare(l, r) != 0; }
};

struct greater {
        compare_three_way compare;
        greater(const State& s) : compare{s} {}
        bool operator()(const val& l, const val& r) const
        { return compare(l, r) > 0; }
};

struct less {
        compare_three_way compare;
        less(const State& s) : compare{s} {}
        bool operator()(const val& l, const val& r) const
        { return compare(l, r) < 0; }
};

struct greater_equal {
        compare_three_way compare;
        greater_equal(const State& s) : compare{s} {}
        bool operator()(const val& l, const val& r) const
        { return compare(l, r) >= 0; }
};

struct less_equal {
        compare_three_way compare;
        less_equal(const State& s) : compare{s} {}
        bool operator()(const val& l, const val& r) const
        { return compare(l, r) <= 0; }
};

inline val& head(State& s, const Ref<val>& r);
inline const val& head(const State& s, const Ref<val>& r);

struct State {
        using sfun_type = std::byte*;
        using cfun_type = val(*)(State&, tuple);

        pool mem;
        std::vector<val> globals;
        std::vector<val> stack;
        std::map<std::string, val> exports;

        // When called as a function, the state is a factory for objects of all
        // supported types, both atomic and complex:

        val operator()() { return mem(data_tag::nil_); }
        val operator()(bool b) { return mem(b ? data_tag::true_ : data_tag::false_); }
        template<std::integral I> val operator()(I n) { return mem(n); }

        // To construct strings, the following options are provided:
        // (iterator<char> first, sentinel last)
        // (range<char>&& r)
        // (const char* p)

        template<std::input_iterator I, std::sentinel_for<I> S>
        val operator()(I first, S last)
        requires std::is_same_v<std::iter_value_t<I>, char>
        { return mem(first, last); }

        template<std::ranges::input_range R>
        val operator()(R&& r)
        requires std::is_same_v<std::ranges::range_value_t<R>, char>
        { return mem(std::forward<R>(r)); }

        val operator()(const char* p)
        { return mem(p); }

        // To construct tuples, the following options are provided:
        // .tuple(Types&&...)
        // (std::tuple<Types...>)
        // (std::pair<A, B>)
        // (std::array<T, N>)

        template<class... Types>
        val tuple(Types&&... types)
        { return mem({mem(types)...}); }

        template<class Tuple, size_t... I>
        val impl(const Tuple& x, std::index_sequence<I...>)
        { return mem({std::get<I>(x)...}); }

        val operator()(const std::pair<val, val>& x)
        { return impl(x, std::make_index_sequence<2>{}); }

        template<class... Types>
        val operator()(const std::tuple<Types...>& x)
        requires std::conjunction_v<std::is_same<std::decay_t<Types>, val>...>
        { return impl(x, std::make_index_sequence<sizeof...(Types)>{}); }

        template<size_t N>
        val operator()(const std::array<val, N>& x)
        { return impl(x, std::make_index_sequence<N>{}); }

        // To construct lists, the following options are provided:
        // .list(Types&&...)
        // (iterator<val> first, sentinel last)
        // (range<val>&& r)
        // (initializer_list<val> list)

        template<class... Types>
        val list(Types&&... types)
        { return (*this)({(*this)(types)...}); }

        template<std::input_iterator I, std::sentinel_for<I> S>
        val operator()(I first, S last)
        requires std::is_same_v<std::iter_value_t<I>, val>
        {
                size_t size = std::distance(first, last);
                auto h = mem(next_power_of_two(size), mem());
                std::ranges::copy(first, last, get_tuple(mem, h).begin());
                return mem(mem(data_tag::list_), h, tail(h, size));
        }

        template<std::ranges::input_range R>
        val operator()(R&& r)
        requires std::is_same_v<std::ranges::range_value_t<R>, val>
        { return (*this)(std::ranges::begin(r), std::ranges::end(r)); }

        val operator()(std::initializer_list<val> list)
        { return (*this)(list.begin(), list.end()); }

        // To construct dictionaries, the following options are provided:
        // .dict(Types&&...)
        // (iterator<pair> first, sentinel last)
        // (range<pair>&& r)
        // (initializer_list<pair> list)

        template<class... Types>
        val dict(Types&&... types)
        {
                return (*this)({std::make_pair((*this)(std::get<0>(types)),
                                        (*this)(std::get<1>(types)))...});
        }

        template<std::input_iterator I, std::sentinel_for<I> S>
        val operator()(I first, S last)
        requires std::is_same_v<std::iter_value_t<I>, pair>
        {
                size_t size = std::distance(first, last);
                auto h = mem(next_power_of_two(size), mem());
                auto t = h;
                for (auto i = first; i != last; ++i, t = tail(t))
                        get_tuple(mem, t)[0] = mem(i->first, i->second);
                return mem(mem(data_tag::dict_), h, t);
        }

        template<std::ranges::input_range R>
        val operator()(R&& r)
        requires std::is_same_v<std::ranges::range_value_t<R>, pair>
        { return (*this)(std::ranges::begin(r), std::ranges::end(r)); }

        val operator()(std::initializer_list<pair> list)
        { return (*this)(list.begin(), list.end()); }

        // Construct functions

        val bind(const val& fun, const val& env)
        { return mem(mem(data_tag::bind_), fun, env); }

        template<std::input_iterator I, std::sentinel_for<I> S>
        val bind(const val& fun, I first, S last)
        requires std::is_same_v<std::iter_value_t<I>, val>
        { return bind(fun, mem(first, last)); }

        template<std::ranges::input_range R>
        val bind(const val& fun, R&& r)
        requires std::is_same_v<std::ranges::range_value_t<R>, val>
        { return bind(fun, std::ranges::begin(r), std::ranges::end(r)); }

        val operator()(sfun_type fun)
        { return mem(mem(data_tag::sfun_), mem(reinterpret_cast<void*>(fun))); }

        val operator()(cfun_type fun)
        { return mem(mem(data_tag::cfun_), mem(reinterpret_cast<void*>(fun))); }

        // Check object type

        bool is_nil(const val& r) const
        { return r.type() == Payload::tag && get_tag(r) == data_tag::nil_; }

        bool is_bool(const val& r) const
        {
                return r.type() == Payload::tag &&
                        (get_tag(r) == data_tag::true_ || get_tag(r) == data_tag::false_);
        }

        bool is_number(const val& r) const
        { return r.type() == Payload::number; }

        bool is_pointer(const val& r) const
        { return r.type() == Payload::pointer; }

        bool is_string(const val& r) const
        { return r.type() == Payload::string; }

        bool is_tuple(const val& r) const
        { return r.type() == Payload::tuple; }

        bool is_object(const val& r) const
        {
                if (r.type() != Payload::tuple || get_tuple(r).empty()) return false;
                auto t = get_tuple(mem, r)[0];
                return t.type() == Payload::tag;
        }

        bool is_object(const val& r, data_tag type) const
        {
                if (r.type() != Payload::tuple || get_tuple(r).empty()) return false;
                auto t = get_tuple(mem, r)[0];
                return t.type() == Payload::tag && get_tag(t) == type;
        }

        bool is_list(const val& r) const
        { return is_object(r, data_tag::list_); }

        bool is_dict(const val& r) const
        { return is_object(r, data_tag::dict_); }

        bool is_sfun(const val& r) const
        { return is_object(r, data_tag::sfun_); }

        bool is_cfun(const val& r) const
        { return is_object(r, data_tag::cfun_); }

        bool is_closure(const val& r) const
        { return is_object(r, data_tag::bind_); }

        // Export global values

        void set(std::string_view name, val&& val)
        { exports.emplace(name, val); }

        template<class... Args>
        void set(std::string_view name, Args&&... args)
        { set(name, (*this)(std::forward<Args>(args)...)); }

        template<class... Args>
        void set_tuple(std::string_view name, Args&&... args)
        { set(name, tuple(std::forward<Args>(args)...)); }

        template<class... Args>
        void set_list(std::string_view name, Args&&... args)
        { set(name, list(std::forward<Args>(args)...)); }

        template<class... Args>
        void set_dict(std::string_view name, Args&&... args)
        { set(name, dict(std::forward<Args>(args)...)); }

        // Access global values

        val get(std::string_view name)
        {
                auto i = exports.find(std::string(name));
                return (i == exports.end()) ? val{} : i->second;
        }
};

// Cast shon objects to a supported type

inline constexpr bool get_bool(const val& l)
{
        switch (get_tag(l)) {
         case data_tag::true_: return true;
         case data_tag::false_: return false;
         default: throw std::bad_cast{};
        }
}

inline auto get_string(const State& s, const val& r)
{ return get_string(s.mem, r); }

// Wrapper for a list that allows treating it as if it was a C++ object

template<bool isConst> struct Seq_iterator {
        using Base = std::conditional_t<isConst, const State*, State*>;
        using value_type = val;
        using reference = std::conditional_t<isConst, const val&, val&>;
        using pointer = std::conditional_t<isConst, const val*, val*>;
        using iterator_category = std::random_access_iterator_tag;

        Base state{nullptr};
        ref pos;

        constexpr reference operator*() const { return head(*state, pos); }
        constexpr pointer operator->() const { return &head(*state, pos); }
        constexpr reference operator[](ptrdiff_t n) const { return head(*state, pos + n); }

        constexpr Seq_iterator& operator++() { ++pos; return *this; }
        constexpr Seq_iterator operator++(int) { auto r = *this; ++*this; return r; }
        constexpr Seq_iterator& operator+=(size_t n) { pos += n; return *this; }
        constexpr Seq_iterator operator+(size_t n) const { return Seq_iterator{state, pos + n}; }

        constexpr Seq_iterator& operator--() { --pos; return *this; }
        constexpr Seq_iterator operator--(int) { auto r = *this; --*this; return r; }
        constexpr Seq_iterator& operator-=(size_t n) { pos -= n; return *this; }
        constexpr Seq_iterator operator-(size_t n) const { return Seq_iterator{state, pos - n}; }

        constexpr ptrdiff_t operator-(const Seq_iterator& r) const { return pos - r.pos; }

        constexpr bool operator==(const Seq_iterator& r) const { return pos == r.pos; }
        constexpr auto operator<=>(const Seq_iterator& r) const { return pos <=> r.pos; }
};

template<bool isConst> class Tuple {
 public:
        using iterator = Seq_iterator<isConst>;

        using Base = typename iterator::Base;
        using value_type = typename iterator::value_type;
        using reference = typename iterator::reference;
        using pointer = typename iterator::pointer;

 private:
        Base state{nullptr};
        ref first;
        ref last;

 public:
        Tuple(Base s, val v) : state{s}, first{get_tuple(v)}, last{first + first.size()} {}

        constexpr iterator begin() const { return iterator{state, first}; }
        constexpr iterator end() const { return iterator{state, last}; }

        constexpr bool empty() const { return end() == begin(); }
        constexpr size_t size() const { return end() - begin(); }

        constexpr reference operator[](size_t n) { return begin()[n]; }
};

inline Tuple<true> get_tuple(const State& s, const val& r)
{ return Tuple<true>(&s, r); }

inline Tuple<false> get_tuple(State& s, const val& r)
{ return Tuple<false>(&s, r); }

template<bool isConst> class List {
 public:
        using iterator = Seq_iterator<isConst>;

        using Base = typename iterator::Base;
        using Tuple = std::conditional_t<isConst, const_tuple, tuple>;
        using value_type = typename iterator::value_type;
        using reference = typename iterator::reference;
        using pointer = typename iterator::pointer;

 private:
        Base state{nullptr};
        Tuple data;

 public:
        List(Base s, val v) : state{s}, data{get_tuple(s->mem, v)}
        {
                if (get_tag(data[0]) != data_tag::list_)
                        throw std::bad_cast{};
        }

        constexpr iterator begin() const { return iterator{state, get_tuple(data[1])}; }
        constexpr iterator end() const { return iterator{state, get_tuple(data[2])}; }

        constexpr bool empty() const { return end() == begin(); }
        constexpr size_t size() const { return end() - begin(); }
        constexpr size_t allocated() const { return get_tuple(data[1]).size(); }

        void reserve(size_t n)
        requires (!isConst)
        {
                if (n <= allocated())
                        return;
                n = next_power_of_two(n);
                size_t s = size();
                val first = (state->mem)(n, val());
                std::ranges::copy(get_tuple(*state, data[1]),
                                get_tuple(*state, first).begin());
                data[1] = first;
                data[2] = get_tuple(first) + s;
        }

        void resize(size_t n)
        requires (!isConst)
        {
                reserve(n);
                data[2] = get_tuple(data[1]) + n;
        }

        reference operator[](size_t n)
        requires (!isConst)
        {
                if (n >= size())
                        resize(n + 1);
                return begin()[n];
        }

        reference operator[](size_t n)
        requires isConst
        {
                if (n < size()) return begin()[n];
                throw std::out_of_range{"shon list accessed out of range"};
        }
};

inline List<false> get_list(State& s, const val& r)
{ return List<false>(&s, r); }

inline List<true> get_list(const State& s, const val& r)
{ return List<true>(&s, r); }

template<bool isConst> class Dict {
 public:
        using iterator = Seq_iterator<isConst>;

        using Base = typename iterator::Base;
        using Tuple = std::conditional_t<isConst, const_tuple, tuple>;
        using value_type = typename iterator::value_type;
        using reference = typename iterator::reference;
        using pointer = typename iterator::pointer;

 private:
        Base state{nullptr};
        Tuple data;

 public:
        Dict(Base s, val v) : state{s}, data{get_tuple(s->mem, v)}
        {
                if (get_tag(data[0]) != data_tag::dict_)
                        throw std::bad_cast{};
        }

        constexpr iterator begin() const { return iterator{state, get_tuple(data[1])}; }
        constexpr iterator end() const { return iterator{state, get_tuple(data[2])}; }

        constexpr bool empty() const { return end() == begin(); }
        constexpr size_t size() const { return end() - begin(); }
        constexpr size_t allocated() const { return get_tuple(data[1]).size(); }

        void reserve(size_t n)
        requires (!isConst)
        {
                if (n <= allocated())
                        return;
                n = next_power_of_two(n);
                size_t s = size();
                val first = (state->mem)(n, val());
                std::ranges::copy(get_tuple(*state, data[1]),
                                get_tuple(*state, first).begin());
                data[1] = first;
                data[2] = get_tuple(first) + s;
        }

        void resize(size_t n)
        requires (!isConst)
        {
                reserve(n);
                data[2] = get_tuple(data[1]) + n;
        }

        reference operator[](val v)
        requires (!isConst)
        {
                equal_to cmp(*state);
                for (auto x : *this) {
                        auto kv = get_tuple(*state, x);
                        if (cmp(kv[0], v)) return kv[1];
                }
                throw std::out_of_range{"shon dict accessed out of range"};
        }

        reference operator[](val v)
        requires isConst
        {
                equal_to cmp(*state);
                for (auto x : *this) {
                        auto kv = get_tuple(*state, x);
                        if (cmp(kv[0], v)) return kv[1];
                }
                throw std::out_of_range{"shon dict accessed out of range"};
        }
};

inline Dict<false> get_dict(State& s, const val& r)
{ return Dict<false>(&s, r); }

inline Dict<true> get_dict(const State& s, const val& r)
{ return Dict<true>(&s, r); }

inline auto get_sfun(const State& s, const val& r)
{
        auto u = get_tuple(s, r);
        if (get_tag(u[0]) != data_tag::sfun_) throw std::bad_cast{};
        return reinterpret_cast<State::sfun_type>(get_pointer(u[1]));
}

inline auto get_cfun(const State& s, const val& r)
{
        auto u = get_tuple(s, r);
        if (get_tag(u[0]) != data_tag::cfun_) throw std::bad_cast{};
        return reinterpret_cast<State::cfun_type>(get_pointer(u[1]));
}

inline auto get_closure(const State& s, const val& r)
{
        auto u = get_tuple(s, r);
        if (get_tag(u[0]) != data_tag::bind_) throw std::bad_cast{};
        return std::make_pair(u[1], u[2]);
}

// Operations on shon objects

inline bool is_true(const val& l)
{
        switch (l.type()) {
         case Payload::end: return false;
         case Payload::tag: return get_tag(l) > data_tag::false_;
         case Payload::number: return get_number(l) != 0;
         case Payload::pointer: return get_pointer(l) != nullptr;
         case Payload::string: return !get_string(l).empty();
         case Payload::tuple: return true;
        }
        abort(); // unreachable
}

inline val operator+(const val& l, const val& r)
{ return val{get_number(l) + get_number(r)}; }

inline val operator-(const val& l, const val& r)
{ return val{get_number(l) - get_number(r)}; }

inline val operator*(const val& l, const val& r)
{ return val{get_number(l) * get_number(r)}; }

inline val operator/(const val& l, const val& r)
{ return val{get_number(l) / get_number(r)}; }

inline val operator%(const val& l, const val& r)
{ return val{get_number(l) % get_number(r)}; }

inline val operator&(const val& l, const val& r)
{ return val{get_number(l) & get_number(r)}; }

inline val operator|(const val& l, const val& r)
{ return val{get_number(l) | get_number(r)}; }

inline val operator^(const val& l, const val& r)
{ return val{get_number(l) ^ get_number(r)}; }

inline val operator<<(const val& l, const val& r)
{ return val{get_number(l) << get_number(r)}; }

inline val operator>>(const val& l, const val& r)
{ return val{get_number(l) >> get_number(r)}; }

inline bool operator&&(const val& l, const val& r)
{ return is_true(l) && is_true(r); }

inline bool operator||(const val& l, const val& r)
{ return is_true(l) || is_true(r); }

inline val operator+(const val& l)
{ return val{get_number(l)}; }

inline val operator-(const val& l)
{ return val{-get_number(l)}; }

inline val operator~(const val& l)
{ return val{~get_number(l)}; }

inline bool operator!(const val& l)
{ return !is_true(l); }

struct Concat {
        State& s;

        // First variant of cat concatenates two tuples

        template<std::input_iterator I1, std::sentinel_for<I1> S1,
                std::input_iterator I2, std::sentinel_for<I2> S2>
        val operator()(I1 f1, S1 l1, I2 f2, S2 l2) const
        requires std::is_same_v<std::iter_value_t<I1>, val> &&
                 std::is_same_v<std::iter_value_t<I2>, val>
        {
                auto mid = std::ranges::distance(f1, l1);
                auto end = mid + std::ranges::distance(f2, l2);
                val buf[end];
                std::copy(f1, l1, buf);
                std::copy(f2, l2, buf + mid);
                return s.mem(buf, buf + end);
        }

        template<std::ranges::input_range R1, std::ranges::input_range R2>
        val operator()(R1&& r1, R2&& r2) const
        requires std::is_same_v<std::ranges::range_value_t<R1>, val> &&
                 std::is_same_v<std::ranges::range_value_t<R2>, val>
        {
                return (*this)(std::ranges::begin(r1), std::ranges::end(r1),
                                std::ranges::begin(r2), std::ranges::end(r2));
        }

        template<std::ranges::input_range R1>
        val operator()(R1&& r1, const val& r2) const
        requires std::is_same_v<std::ranges::range_value_t<R1>, val>
        { return (*this)(r1, get_tuple(s, r2)); }

        template<std::ranges::input_range R2>
        val operator()(const val& r1, R2&& r2) const
        requires std::is_same_v<std::ranges::range_value_t<R2>, val>
        { return (*this)(get_tuple(s, r1), r2); }

        // Second variant of cat concatenates two strings

        template<std::input_iterator I1, std::sentinel_for<I1> S1,
                std::input_iterator I2, std::sentinel_for<I2> S2>
        val operator()(I1 f1, S1 l1, I2 f2, S2 l2) const
        requires std::is_same_v<std::iter_value_t<I1>, char> &&
                 std::is_same_v<std::iter_value_t<I2>, char>
        {
                auto mid = std::ranges::distance(f1, l1);
                auto end = mid + std::ranges::distance(f2, l2);
                char buf[end];
                std::copy(f1, l1, buf);
                std::copy(f2, l2, buf + mid);
                return s.mem(buf, buf + end);
        }

        template<std::ranges::input_range R1, std::ranges::input_range R2>
        val operator()(R1&& r1, R2&& r2) const
        requires std::is_same_v<std::ranges::range_value_t<R1>, char> &&
                 std::is_same_v<std::ranges::range_value_t<R2>, char>
        {
                return (*this)(std::ranges::begin(r1), std::ranges::end(r1),
                                std::ranges::begin(r2), std::ranges::end(r2));
        }

        template<std::ranges::input_range R1>
        val operator()(R1&& r1, const val& r2) const
        requires std::is_same_v<std::ranges::range_value_t<R1>, char>
        { return (*this)(r1, get_string(s.mem, r2)); }

        template<std::ranges::input_range R2>
        val operator()(const val& r1, R2&& r2) const
        requires std::is_same_v<std::ranges::range_value_t<R2>, char>
        { return (*this)(get_string(s.mem, r1), r2); }

        // Concatenate lists

        val list(const val& r1, const val& r2) const
        {
                not_equal_to cmp(s);
                std::vector<val> buf;
                for (auto x : get_list(s, r1))
                        buf.push_back(x);
                for (auto x : get_list(s, r2))
                        buf.push_back(x);
                return s(buf);
        }

        // Concatenate dictionaries

        val dict(const val& r1, const val& r2) const
        {
                not_equal_to cmp(s);
                std::vector<pair> buf;
                for (auto x : get_dict(s, r1)) {
                        auto y = get_tuple(s, x);
                        buf.push_back(std::make_pair(y[0], y[1]));
                }
                for (auto x : get_dict(s, r2)) {
                        auto y = get_tuple(s, x);
                        buf.push_back(std::make_pair(y[0], y[1]));
                }
                return s(buf);
        }

        // If no types are specified, they are deduced from the inputs

        val operator()(const val& r1, const val& r2) const
        {
                if (r1.type() != r2.type())
                        throw std::bad_cast{};
                if (s.is_number(r1))
                        return s(get_number(r1) + get_number(r2));
                if (s.is_string(r1))
                        return (*this)(get_string(s.mem, r1), get_string(s.mem, r2));
                if (s.is_list(r1))
                        return list(r1, r2);
                if (s.is_dict(r1))
                        return dict(r1, r2);
                if (s.is_tuple(r1))
                        return (*this)(get_tuple(s, r1), get_tuple(s, r2));
                throw std::bad_cast{};
        }
};

inline Concat concat(State& s) { return Concat{s}; }

template<class... Args>
val concat(State& s, Args&&... args)
{ return Concat{s}(std::forward<Args>(args)...); }

struct State_ostream {
        std::ostream& os;
        const State& s;
        int depth = 0;

        State_ostream(std::ostream& os, const State& s) : os{os}, s{s} {}

        State_ostream& operator<<(const val& r)
        {
                if (s.is_list(r))
                        list(r);
                else if (s.is_dict(r))
                        dict(r);
                else
                        visit(s.mem)([this](const auto& x){ *this << x; }, r);
                return *this;
        }

        State_ostream& operator<<(std::nullopt_t)
        {
                os << "`";
                return *this;
        }

        State_ostream& operator<<(std::string_view s)
        {
                os << '"' << unescape(s) << '"';
                return *this;
        }

        template<std::ranges::input_range R>
        State_ostream& operator<<(const R& r)
        requires std::is_same_v<std::ranges::range_value_t<R>, val>
        {
                if (depth > 0) {
                        os << "(+" << r.size() << ")";
                        return *this;
                }
                ++depth;
                os << '(';
                const char* separator = "";
                for (const auto& x : r) {
                        *this << separator << x;
                        separator = " ";
                }
                os << ')';
                --depth;
                return *this;
        }

        void list(const val& r)
        {
                if (depth > 0) {
                        os << "[*]";
                        return;
                }
                ++depth;
                os << '[';
                const char* separator = "";
                for (auto x : get_list(s, r)) {
                        *this << separator << x;
                        separator = " ";
                }
                os << ']';
                --depth;
        }

        void dict(const val& r)
        {
                if (depth > 0) {
                        os << "{*}";
                        return;
                }
                ++depth;
                os << '{';
                const char* separator = "";
                for (auto x : get_dict(s, r)) {
                        auto u = get_tuple(s, x);
                        *this << separator << u[0] << ":" << u[1];
                        separator = " ";
                }
                os << '}';
                --depth;
        }

        template<class T> State_ostream& operator<<(const T& x)
        {
                os << x;
                return *this;
        }
};

inline State_ostream operator<<(std::ostream& os, const State& s)
{ return State_ostream{os, s}; }

inline std::string to_string(const State& s, const val& r)
{
        std::stringstream solution;
        solution << s << r;
        return solution.str();
}

inline std::partial_ordering compare_three_way::operator()(const val& l, const val& r) const
{
        if (l.type() != r.type())
                return l.type() <=> r.type();
        if (l.type() == Payload::tag)
                return get_tag(l) <=> get_tag(r);
        if (l.type() == Payload::number)
                return get_number(l) <=> get_number(r);
        if (l.type() == Payload::pointer)
                return get_pointer(l) <=> get_pointer(r);
        if (l.type() == Payload::string)
                return get_string(s.mem, l) <=> get_string(s.mem, r);
        if (l.type() == Payload::tuple)
                return get_tuple(l) <=> get_tuple(r);
        return std::partial_ordering::unordered;
}

inline val& head(State& s, const Ref<val>& r) { return head(s.mem, r); }
inline const val& head(const State& s, const Ref<val>& r) { return head(s.mem, r); }

template<class T> class cast {
 private:
        const pool& t;

 public:
        cast(const pool& t) : t{t} {}

        auto operator()(const val& r) const
        {
                using U = std::decay_t<T>;
                if constexpr (std::is_same_v<U, val>)
                        return r;
                else if constexpr (std::is_same_v<U, bool>)
                        return is_true(r);
                else if constexpr (std::is_same_v<U, data_tag>)
                        return get_tag(r);
                else if constexpr (std::is_integral_v<U>)
                        return get_number(r);
                else if constexpr (std::is_same_v<U, std::string_view>)
                        return get_string(t, r);
                else if constexpr (std::is_same_v<U, std::string>)
                        return get_string(t, r);
                else
                        throw std::bad_cast{};
        }
};

template<class... Types> class invoke {
 private:
        const pool& t;

 public:
        invoke(const pool& t) : t{t} {}

        template<class F, class... Args>
        auto operator()(F&& fun, Args&&... args) const
        requires std::conjunction_v<std::is_same<std::decay_t<Args>, val>...>
        { return std::invoke(std::forward<F>(fun), cast<Types>(t)(args)...); }
};

template<class... Types> class apply {
 private:
        static constexpr const size_t N = sizeof...(Types);

        const pool& t;

        template<class F, class Tuple, size_t... I>
        auto impl(F&& fun, Tuple&& tuple, std::index_sequence<I...>) const
        {
                return std::invoke(std::forward<F>(fun),
                                cast<Types>(t)(std::get<I>(tuple))...);
        }

 public:
        apply(const pool& t) : t{t} {}

        template<class F, class Tuple>
        auto operator()(F&& fun, Tuple&& tuple) const
        {
                return impl(std::forward<F>(fun), std::forward<Tuple>(tuple),
                                std::make_index_sequence<N>{});
        }
};

template<class... Types> class call {
 private:
        static constexpr const size_t N = sizeof...(Types);

        const pool& t;

        template<class F, std::input_iterator In, size_t... I>
        auto impl(F&& fun, In first, std::index_sequence<I...>) const
        { return std::invoke(std::forward<F>(fun), cast<Types>(t)(*std::next(first, I))...); }

 public:
        call(const pool& t) : t{t} {}

        template<class F, std::input_iterator In>
        auto operator()(F&& fun, In first) const
        { return impl(std::forward<F>(fun), first, std::make_index_sequence<N>{}); }

        template<class F, std::ranges::input_range R>
        auto operator()(F&& fun, R&& r) const
        {
                if (r.size() < N)
                        throw std::out_of_range{"Not enough arguments to invoke function"};
                return (*this)(std::forward<F>(fun), std::ranges::begin(r));
        }

        template<class F>
        auto operator()(F&& fun, std::initializer_list<val> list)
        {
                if (list.size() < N)
                        throw std::out_of_range{"Not enough arguments to invoke function"};
                return (*this)(std::forward<F>(fun), list.begin());
        }
};

template<class F, F function, class... Args> struct wrap {
        static val fun(State& s, tuple args)
        {
                if constexpr (std::is_void_v<std::invoke_result_t<F, Args...>>) {
                        call<Args...>(s.mem)(function, args);
                        return s();
                } else {
                        return s(call<Args...>(s.mem)(function, args));
                }
        }
};

template<class F, F fun, class... Args>
static constexpr const auto wrap_f = wrap<F, fun, Args...>::fun;

struct begin {
        State& s;

        Ref<val> operator()(const val& r) const
        {
                if (s.is_list(r))
                        return get_tuple(get_tuple(s, r)[1]);
                if (s.is_dict(r))
                        return get_tuple(get_tuple(s, r)[1]);
                if (s.is_tuple(r))
                        return get_tuple(r);
                throw std::bad_cast{};
        }
};

struct end {
        State& s;

        Ref<val> operator()(const val& r) const
        {
                if (s.is_list(r))
                        return get_tuple(get_tuple(s, r)[2]);
                if (s.is_dict(r))
                        return get_tuple(get_tuple(s, r)[2]);
                if (s.is_tuple(r)) {
                        auto t = get_tuple(r);
                        return Ref<val>(t.last, t.last);
                }
                throw std::bad_cast{};
        }
};

struct size {
        State& s;

        size_t operator()(const val& r) const
        {
                if (s.is_list(r))
                        return get_list(s, r).size();
                if (s.is_dict(r))
                        return get_dict(s, r).size();
                if (s.is_tuple(r))
                        return get_tuple(r).size();
                return 1;
        }
};

inline constexpr val next(const val& r, size_t n = 1)
{ return get_tuple(r) + n; }

inline constexpr void advance(val& r, size_t n = 1)
{ r = next(r, n); }

inline const val& access(const State& p, const val& r, const val& i)
{
        if (p.is_list(r))
                return get_list(p, r)[get_number(i)];
        if (p.is_dict(r))
                return get_dict(p, r)[i];
        if (p.is_tuple(r))
                return get_tuple(p, r)[get_number(i)];
        throw std::bad_cast{};
}

inline val& access(State& p, const val& r, const val& i)
{
        if (p.is_list(r))
                return get_list(p, r)[get_number(i)];
        if (p.is_dict(r))
                return get_dict(p, r)[i];
        if (p.is_tuple(r))
                return get_tuple(p, r)[get_number(i)];
        throw std::bad_cast{};
}

}; // namespace shon

#endif // SHON_VAL_H_
