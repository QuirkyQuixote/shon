
#ifndef SHON_TREE_H_
#define SHON_TREE_H_

#include <cstring>

#include <iterator>
#include <algorithm>
#include <ranges>
#include <string_view>
#include <vector>
#include <set>
#include <map>
#include <ostream>
#include <sstream>

namespace shon {

enum class Payload { end, tag, number, pointer, string, tuple };

template<class T> struct Ref {
        uint32_t first{0};
        uint32_t last{0};

        constexpr Ref() = default;
        constexpr Ref(uint32_t first, uint32_t last) : first{first}, last{last} {}

        constexpr Ref& operator++() { ++first; return *this; }
        constexpr Ref operator++(int) { auto r = *this; ++*this; return r; }
        constexpr Ref& operator+=(size_t n) { first += n; return *this; }
        constexpr Ref operator+(size_t n) const { return Ref(first + n, last); }

        constexpr Ref& operator--() { --first; return *this; }
        constexpr Ref operator--(int) { auto r = *this; --*this; return r; }
        constexpr Ref& operator-=(size_t n) { first -= n; return *this; }
        constexpr Ref operator-(size_t n) const { return Ref(first - n, last); }

        constexpr ptrdiff_t operator-(const Ref& r) const { return first - r.first; }

        constexpr bool empty() const { return first == last; }
        constexpr size_t size() const { return last - first; }
};

template<class T> constexpr std::partial_ordering operator<=>(const Ref<T>& l, const Ref<T>& r)
{
        if (l.last != r.last) return std::partial_ordering::unordered;
        return l.first <=> r.first;
}

template<class T> constexpr bool operator==(const Ref<T>& l, const Ref<T>& r)
{ return (l.first == r.first && l.last == r.last); }

template<class Tag> class Val {
 public:
        using tag_type = Tag;
        using number_type = int64_t;
        using pointer_type = void*;
        using string_type = Ref<char>;
        using tuple_type = Ref<Val<Tag>>;

 private:
        union {
                tag_type tag_;
                number_type number_;
                pointer_type pointer_;
                string_type string_;
                tuple_type tuple_;
        };

        Payload type_;

        constexpr void type_check_(Payload p) const
        { if (type_ != p) throw std::bad_cast{}; }

 public:
        constexpr Val() { type_ = Payload::end; }
        constexpr Val(tag_type t) { tag_ = t; type_ = Payload::tag; }
        template<std::integral I> constexpr Val(I i) { number_ = i; type_ = Payload::number; }
        constexpr Val(pointer_type p) { pointer_ = p; type_ = Payload::pointer; }
        constexpr Val(string_type s) { string_ = s; type_ = Payload::string; }
        constexpr Val(tuple_type t) { tuple_ = t; type_ = Payload::tuple; }

        constexpr Val(const Val& other) = default;
        constexpr Val(Val&& other) = default;

        constexpr Val& operator=(const Val& other) = default;
        constexpr Val& operator=(Val&& other) = default;

        ~Val() = default;

        constexpr Payload type() const { return type_; }
        constexpr operator bool() const { return type_ != Payload::end; }

        friend Tag get_tag<Tag>(const Val& t);
        friend int64_t get_number<Tag>(const Val& t);
        friend void* get_pointer<Tag>(const Val& t);
        friend Ref<char> get_string<Tag>(const Val& t);
        friend Ref<Val<Tag>> get_tuple<Tag>(const Val& t);
};

template<class Tag> constexpr void val_type_check_(const Val<Tag>& t, Payload p)
{ if (t.type() != p) throw std::bad_cast{}; }

template<class Tag> constexpr Tag get_tag(const Val<Tag>& t)
{ val_type_check_(t, Payload::tag); return t.tag_; }

template<class Tag> constexpr int64_t get_number(const Val<Tag>& t)
{ val_type_check_(t, Payload::number); return t.number_; }

template<class Tag> constexpr void* get_pointer(const Val<Tag>& t)
{ val_type_check_(t, Payload::pointer); return t.pointer_; }

template<class Tag> constexpr Ref<char> get_string(const Val<Tag>& t)
{ val_type_check_(t, Payload::string); return t.string_; }

template<class Tag> constexpr Ref<Val<Tag>> get_tuple(const Val<Tag>& t)
{ val_type_check_(t, Payload::tuple); return t.tuple_; }

template<class T> struct Pool {
        std::vector<T> data;

        void clear() { data.clear(); }

        Ref<T> make(size_t count, const T& value)
        {
                if (count == 0) return Ref<T>();
                auto offset = data.size();
                data.insert(data.end(), count, value);
                return Ref<T>(offset, offset + count);
        }

        template<std::input_iterator I, std::sentinel_for<I> S>
        Ref<T> make(I first, S last)
        requires std::is_same_v<std::iter_value_t<I>, T>
        {
                if (first == last) return Ref<T>();
                auto offset = data.size();
                auto count = std::ranges::distance(first, last);
                data.insert(data.end(), first, last);
                return Ref<T>(offset, offset + count);
        }

        template<std::ranges::input_range R>
        Ref<T> make(R r)
        requires std::is_same_v<std::ranges::range_value_t<R>, T>
        { return make(std::ranges::begin(r), std::ranges::end(r)); }

        T* get(size_t n) { return data.data() + n; }
        const T* get(size_t n) const { return data.data() + n; }
};

template<class Tag> class Tree {
 public:
        using val = Val<Tag>;
        using tag_type = typename val::tag_type;
        using number_type = typename val::number_type;
        using pointer_type = typename val::pointer_type;
        using string_type = typename val::string_type;
        using tuple_type = typename val::tuple_type;

 private:
        Pool<char> string_pool;
        Pool<val> tuple_pool;

 public:
        constexpr val operator()() { return val{}; }
        constexpr val operator()(tag_type t) { return val{t}; }
        template<std::integral I> val constexpr operator()(I i) { return val{i}; }
        constexpr val operator()(pointer_type p) { return val{p}; }

        val operator()(const char* s)
        { return val{string_pool.make(s, s + strlen(s))}; }

        val operator()(size_t count, char value)
        { return val{string_pool.make(count, value)}; }

        template<std::input_iterator I, std::sentinel_for<I> S>
        val operator()(I first, S last)
        requires std::is_same_v<std::iter_value_t<I>, char>
        { return val{string_pool.make(first, last)}; }

        val operator()(size_t count, const val& value)
        { return val{tuple_pool.make(count, value)}; }

        template<std::input_iterator I, std::sentinel_for<I> S>
        val operator()(I first, S last)
        requires std::is_same_v<std::iter_value_t<I>, val>
        { return val{tuple_pool.make(first, last)}; }

        template<std::ranges::input_range R> val operator()(R&& r)
        { return (*this)(std::ranges::begin(r), std::ranges::end(r)); }

        val operator()(std::initializer_list<val> l)
        { return (*this)(l.begin(), l.end()); }

        val operator()(const val& other) { return other; }

        template<class... Vals>
        val operator()(Vals&&... vals)
        requires std::conjunction_v<std::is_same<std::decay_t<Vals>, val>...>
        { return (*this)({vals...}); }

        void clear()
        {
                string_pool.clear();
                tuple_pool.clear();
        }

        std::vector<char>& chars() { return string_pool.data; }
        const std::vector<char>& chars() const { return string_pool.data; }
        std::vector<val>& vals() { return tuple_pool.data; }
        const std::vector<val>& vals() const { return tuple_pool.data; }

};

template<class Tag>
std::string_view get_string(const Tree<Tag>& t, const Val<Tag>& r)
{
        auto data = t.chars().data();
        auto bounds = get_string(r);
        return std::string_view(data + bounds.first, data + bounds.last);
}

template<class Tag>
std::ranges::subrange<Val<Tag>*> get_tuple(Tree<Tag>& t, const Val<Tag>& r)
{
        auto data = t.vals().data();
        auto bounds = get_tuple(r);
        return std::ranges::subrange(data + bounds.first, data + bounds.last);
}

template<class Tag>
std::ranges::subrange<const Val<Tag>*> get_tuple(const Tree<Tag>& t, const Val<Tag>& r)
{
        auto data = t.vals().data();
        auto bounds = get_tuple(r);
        return std::ranges::subrange(data + bounds.first, data + bounds.last);
}

template<class Tag> Val<Tag> tail(const Val<Tag>& r, size_t n = 1)
{ return Val<Tag>(get_tuple(r) + n); }

template<class Tag> Val<Tag>& head(Tree<Tag>& p, const Ref<Val<Tag>>& r)
{
        if (!r.empty()) return p.vals().at(r.first);
        throw std::out_of_range{"shon tuple accessed out of range"};
}

template<class Tag> const Val<Tag>& head(const Tree<Tag>& p, const Ref<Val<Tag>>& r)
{
        if (!r.empty()) return p.vals().at(r.first);
        throw std::out_of_range{"shon tuple accessed out of range"};
}


template<class Tag> bool is_object(const Tree<Tag>& t, const Val<Tag>& r, Tag type)
{
        return r.type() == Payload::tuple &&
                get_tuple(t, r)[0].type() == Payload::tag &&
                get_tag(get_tuple(t, r)[0]) == type;
}

template<class Tag> struct Visit {
        using tree = Tree<Tag>;
        using val = Val<Tag>;

        const tree& t;

        template<class Visitor, class... Types>
        auto impl(Visitor&& vis, std::tuple<Types...>&& tuple) const
        { return std::apply(vis, tuple); }

        template<class Visitor, class... Types, class... Vals>
        auto impl(Visitor&& vis, std::tuple<Types...>&& tuple, const val& r, Vals&&... vals) const
        requires std::conjunction_v<std::is_same<std::decay_t<Vals>, val>...>
        {
                switch (r.type()) {
                 case Payload::end:
                        return impl(std::forward<Visitor>(vis),
                                        std::tuple_cat(tuple, std::make_tuple(std::nullopt)),
                                        std::forward<Vals>(vals)...);
                 case Payload::tag:
                        return impl(std::forward<Visitor>(vis),
                                        std::tuple_cat(tuple, std::make_tuple(get_tag(r))),
                                        std::forward<Vals>(vals)...);
                 case Payload::number:
                        return impl(std::forward<Visitor>(vis),
                                        std::tuple_cat(tuple, std::make_tuple(get_number(r))),
                                        std::forward<Vals>(vals)...);
                 case Payload::pointer:
                        return impl(std::forward<Visitor>(vis),
                                        std::tuple_cat(tuple, std::make_tuple(get_pointer(r))),
                                        std::forward<Vals>(vals)...);
                 case Payload::string:
                        return impl(std::forward<Visitor>(vis),
                                        std::tuple_cat(tuple, std::make_tuple(get_string(t, r))),
                                        std::forward<Vals>(vals)...);
                 case Payload::tuple:
                        return impl(std::forward<Visitor>(vis),
                                        std::tuple_cat(tuple, std::make_tuple(get_tuple(t, r))),
                                        std::forward<Vals>(vals)...);
                }
        }

        template<class Visitor, class... Vals>
        auto operator()(Visitor&& vis, Vals&&... vals) const
        requires std::conjunction_v<std::is_same<std::decay_t<Vals>, val>...>
        {
                return impl(std::forward<Visitor>(vis), std::tuple<>{},
                                std::forward<Vals>(vals)...);
        }
};

template<class Tag> Visit<Tag> visit(const Tree<Tag>& t) { return Visit<Tag>(t); }

inline std::string escape(std::string_view in)
{
        std::string s;
        for (auto p = in.begin(); p != in.end(); ++p) {
                if (*p != '\\') {
                        s.push_back(*p);
                        continue;
                }
                switch (*++p) {
                 case '\'': s.push_back('\''); continue;
                 case '"': s.push_back('"'); continue;
                 case '\\': s.push_back('\\'); continue;
                 case 'a': s.push_back('\a'); continue;
                 case 'b': s.push_back('\b'); continue;
                 case 'f': s.push_back('\f'); continue;
                 case 'n': s.push_back('\n'); continue;
                 case 'r': s.push_back('\r'); continue;
                 case 't': s.push_back('\t'); continue;
                 case 'v': s.push_back('\v'); continue;
                 case 'e': s.push_back('\e'); continue;
                 case '0': s.push_back('\0'); continue;
                 default: throw std::runtime_error{"Unrecognized escape sequence"};
                }
        }
        return s;
}

inline std::string unescape(std::string_view in)
{
        std::string s;
        for (auto p = in.begin(); p != in.end(); ++p) {
                switch (*p) {
                 case '\'': s += "\\'"; continue;
                 case '"': s += "\\\""; continue;
                 case '\\': s += "\\\\"; continue;
                 case '\a': s += "\\a"; continue;
                 case '\b': s += "\\b"; continue;
                 case '\f': s += "\\f"; continue;
                 case '\n': s += "\\n"; continue;
                 case '\r': s += "\\r"; continue;
                 case '\t': s += "\\t"; continue;
                 case '\v': s += "\\v"; continue;
                 case '\e': s += "\\e"; continue;
                 case '\0': s += "\\0"; continue;
                 default: s += *p; continue;
                }
        }
        return s;
}

template<class Tag> struct Tree_ostream {
        using tree = shon::Tree<Tag>;
        using val = shon::Val<Tag>;
        using tuple_type = typename tree::tuple_type;

        std::ostream& os;
        Visit<Tag> visit;

        Tree_ostream(std::ostream& os, const tree& t) : os{os}, visit{t} {}

        Tree_ostream& operator<<(const val& r)
        {
                visit([this](const auto& x){ *this << x; }, r);
                return *this;
        }

        Tree_ostream& operator<<(std::nullopt_t)
        {
                os << "'";
                return *this;
        }

        Tree_ostream& operator<<(std::string_view s)
        {
                os << '"' << unescape(s) << '"';
                return *this;
        }

        template<std::ranges::input_range R>
        Tree_ostream& operator<<(const R& r)
        requires std::is_same_v<std::ranges::range_value_t<R>, val>
        {
                os << '(';
                if (!r.empty()) {
                        auto i = r.begin();
                        *this << *i;
                        while (++i != r.end())
                                *this << ' ' << *i;
                }
                os << ')';
                return *this;
        }

        template<class T> Tree_ostream& operator<<(const T& x)
        {
                os << x;
                return *this;
        }
};

template<class Tag>
Tree_ostream<Tag> operator<<(std::ostream& os, const shon::Tree<Tag>& tree)
{ return Tree_ostream<Tag>{os, tree}; }

template<class Tag>
std::string to_string(const Tree<Tag>& t, const Val<Tag>& r)
{
        std::stringstream solution;
        solution << t << r;
        return solution.str();
}

}; // namespace shon

#endif // SHON_TREE_H_
