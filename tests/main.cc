
#include <unistd.h>

#include <iostream>
#include <fstream>
#include <iomanip>

#include "shon/shon.h"

#define SHON_WRAP_F(f,...) shon::wrap_f<decltype(&f), &f, ##__VA_ARGS__>

struct Export_all {
        static int random_number()
        { return 42; }

        static shon::val string(shon::State& s, shon::tuple args)
        {
                std::stringstream solution;
                for (auto r : args)
                        solution << s << r;
                return s(solution.str());
        }

        static shon::val print(shon::State& s, shon::tuple args)
        {
                for (auto r : args) {
                        if (s.is_string(r)) std::cout << get_string(s, r);
                        else std::cout << s << r;
                }
                return s();
        }

        static void assert(bool b)
        { if (!b) throw std::runtime_error{"assertion failed"}; }

        static shon::val gc(shon::State& s, shon::tuple args)
        {
                return s(0);
        }

        static std::string object_name(shon::State& s, const shon::val& r)
        {
                std::stringstream solution;
                solution << get_tag(get_tuple(s, r)[0]);
                return solution.str();
        }

        static shon::val type(shon::State& s, shon::tuple args)
        {
                if (s.is_nil(args[0]))
                        return s("nil");
                if (s.is_bool(args[0]))
                        return s("bool");
                if (s.is_number(args[0]))
                        return s("number");
                if (s.is_pointer(args[0]))
                        return s("pointer");
                if (s.is_string(args[0]))
                        return s("string");
                if (s.is_object(args[0]))
                        return s(object_name(s, args[0]));
                if (s.is_tuple(args[0]))
                        return s("tuple");
                throw std::bad_cast{};
        }

        void operator()(shon::State& s) const
        {
                s.set("the_answer", 42);
                s.set("random_number", SHON_WRAP_F(random_number));
                s.set("print", print);
                s.set("assert", SHON_WRAP_F(assert, bool));
                s.set("string", string);
                s.set("gc", gc);
                s.set("type", type);
                s.set_list("a_list", 1, 2, 3);
                s.set_dict("a_dict",
                                std::make_pair("foo", 1),
                                std::make_pair("bar", 2),
                                std::make_pair("baz", 3));
        }
};

static constexpr const Export_all export_all;

void parse_stream(int argc, char* argv[])
{
        shon::State s;
        export_all(s);
        if (argc) shon::load(s, argv[0]);
        else shon::load(s);
        auto ret = shon::run(s, s.globals[0]);
        std::cout << shon::colorize(shon::to_string(s, ret)) << "\n";
}

void parse_arguments(int argc, char* argv[])
{
        for (;;) {
                switch (getopt(argc, argv, "apfciv")) {
                 case 'a': shon::config.verbose_parser = true; break;
                 case 'p': shon::config.verbose_pruner = true; break;
                 case 'f': shon::config.verbose_flattener = true; break;
                 case 'c': shon::config.verbose_compiler = true; break;
                 case 'i': shon::config.verbose_interpreter = true; break;
                 case 'v': shon::config.verbose_parser = true;
                           shon::config.verbose_pruner = true;
                           shon::config.verbose_flattener = true;
                           shon::config.verbose_compiler = true;
                           shon::config.verbose_interpreter = true;
                           break;
                 case -1: return;
                 default: break;
                }
        }
}

int main(int argc, char* argv[])
{
        try {
                parse_arguments(argc, argv);
                parse_stream(argc - optind, argv + optind);
                return EXIT_SUCCESS;
        } catch (std::exception& ex) {
                std::cerr << "\e[0;31m" << ex.what() << "\e[0;0m\n";
                return EXIT_FAILURE;
        }
}
