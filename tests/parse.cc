
#include <unistd.h>

#include <filesystem>
#include <iostream>

#include "shon/parser.hh"
#include "shon/color.h"

void load()
{
        auto [ast, root] = shon::parse();
        std::cout << "Original AST\n";
        std::cout << shon::colorize(shon::to_string(ast, root)) << "\n";
}

void load(const std::filesystem::path& path)
{
        FILE* stream = fopen(path.c_str(), "r");
        if (stream == nullptr) {
                std::stringstream message;
                message << path << ": " << strerror(errno);
                throw std::runtime_error{message.str()};
        }
        yyrestart(stream);
        load();
        fclose(stream);
}

void parse_stream(int argc, char* argv[])
{
        if (argc) load(argv[0]);
        else load();
}

void parse_arguments(int argc, char* argv[])
{
        for (;;) {
                switch (getopt(argc, argv, "")) {
                 case -1: return;
                 default: break;
                }
        }
}

int main(int argc, char* argv[])
{
        try {
                parse_arguments(argc, argv);
                parse_stream(argc - optind, argv + optind);
                return EXIT_SUCCESS;
        } catch (std::exception& ex) {
                std::cerr << "\e[0;31m" << ex.what() << "\e[0;0m\n";
                return EXIT_FAILURE;
        }
}
