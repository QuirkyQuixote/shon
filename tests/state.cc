
#include <iostream>

#include "shon/state.h"

std::ostream& operator<<(std::ostream& os, std::partial_ordering x)
{
        if (x == std::partial_ordering::less) return os << "less";
        if (x == std::partial_ordering::equivalent) return os << "equivalent";
        if (x == std::partial_ordering::greater) return os << "greater";
        if (x == std::partial_ordering::unordered) return os << "unordered";
        abort(); // unreachable
}

int main(int argc, char* argv[])
{
        shon::State s;

        std::cout << "=== construct atoms\n";
        std::cout << s << s() << "\n";
        std::cout << s << s(false) << "\n";
        std::cout << s << s(true) << "\n";
        std::cout << s << s(0) << "\n";
        std::cout << s << s(1) << "\n";
        std::cout << s << s("hello, world") << "\n";

        std::cout << "=== shon::compare_three_way\n";
        std::cout << shon::compare_three_way(s)(s(0), s(1)) << "\n";
        std::cout << shon::compare_three_way(s)(s(1), s(0)) << "\n";
        std::cout << shon::compare_three_way(s)(s("foo"), s("bar")) << "\n";
        std::cout << shon::compare_three_way(s)(s("bar"), s("foo")) << "\n";

        std::cout << "=== shon::less\n";
        std::cout << shon::less(s)(s(0), s(1)) << "\n";
        std::cout << shon::less(s)(s(1), s(0)) << "\n";
        std::cout << shon::less(s)(s("foo"), s("bar")) << "\n";
        std::cout << shon::less(s)(s("bar"), s("foo")) << "\n";

        std::cout << "=== construct aggregates\n";
        std::cout << s << s.tuple(0, 1, 2) << "\n";
        std::cout << s << s.list(0, 1, 2) << "\n";
        std::cout << s << s.dict(std::make_pair("a", 0),
                        std::make_pair("b", 1),
                        std::make_pair("c", 2)) << "\n";
}
