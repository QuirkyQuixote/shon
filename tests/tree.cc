
#include <iostream>

#include "shon/state.h"

int main(int argc, char* argv[])
{
        shon::pool t;

        std::cout << t << t(0) << "\n";
        std::cout << t << t(1) << "\n";
        std::cout << t << t(shon::data_tag::nil_) << "\n";
        std::cout << t << t(shon::data_tag::true_) << "\n";
        std::cout << t << t(shon::data_tag::false_) << "\n";
        std::cout << t << t("hello, world") << "\n";
        std::cout << t << t(t(0), t(1), t(2)) << "\n";
        std::cout << t << t(t(t("a"), t(0)), t(t("b"), t(1)), t(t("c"), t(2))) << "\n";
}
